/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projetMCMPRO.Vue;

import com.projetMCMPRO.Controleur.ControleurOffrande;
import com.projetMCMPRO.Dal.mysqlconnect;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.swing.table.TableModel;
import net.proteanit.sql.DbUtils;

/**
 *
 * @author INGSAMUEL
 */
public class FenOffrande extends javax.swing.JInternalFrame {

    public static Connection conn;
    public static Statement st; 
    private static final long serialVersionUID = 1L;
    public ResultSet rs;
    public PreparedStatement pst = null;
    public java.sql.Date sqldateOff;
     public static  boolean existNO;
     public String codeMemb, co;
   public  String Numoff;
   public ControleurOffrande ctrlO=new ControleurOffrande();
   String dateoffrande;
    public TableModel model;
    public int indexligne;
    public FenOffrande() {
        initComponents();
        existNO=true;
        conn=mysqlconnect.ConnectDB();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        txtnomservice = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtmontant = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        dtpdateoff = new com.toedter.calendar.JDateChooser();
        cmbmonaie = new javax.swing.JComboBox<String>();
        jLabel5 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        btnajouter = new javax.swing.JButton();
        btnannuler = new javax.swing.JButton();
        btnmodifier = new javax.swing.JButton();
        btnquitter = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tboffrande = new javax.swing.JTable();
        txtrechercher = new javax.swing.JTextField();
        cmboff = new javax.swing.JComboBox();
        lbloffrande1 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        lbloffHtg = new javax.swing.JLabel();
        lbloffusd = new javax.swing.JLabel();

        setClosable(true);
        setTitle("Offrande");
        addInternalFrameListener(new javax.swing.event.InternalFrameListener() {
            public void internalFrameActivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosed(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosing(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameClosing(evt);
            }
            public void internalFrameDeactivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeiconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameIconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameOpened(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameOpened(evt);
            }
        });

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel1.setText("Nom Service :");

        jLabel2.setText("Montant :");

        txtmontant.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtmontantKeyTyped(evt);
            }
        });

        jLabel3.setText("Date :");

        dtpdateoff.setDateFormatString("dd-MM-yyyy");

        cmbmonaie.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "HTG", "USD", "CAD", "EUR", "DOP", "BRL" }));

        jLabel5.setText("Montant :");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel1)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(cmbmonaie, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtnomservice, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(47, 47, 47)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtmontant)
                    .addComponent(dtpdateoff, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(23, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtnomservice, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel1))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel5)
                            .addComponent(cmbmonaie, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(txtmontant, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel3)
                            .addComponent(dtpdateoff, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(6, 6, 6))
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Actions"));

        btnajouter.setText("Ajouter");
        btnajouter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnajouterActionPerformed(evt);
            }
        });

        btnannuler.setText("Annuler");

        btnmodifier.setText("Modifier");
        btnmodifier.setEnabled(false);
        btnmodifier.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnmodifierActionPerformed(evt);
            }
        });

        btnquitter.setText("Quitter");
        btnquitter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnquitterActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnajouter, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnannuler, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnmodifier, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnquitter, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnajouter)
                .addGap(15, 15, 15)
                .addComponent(btnmodifier)
                .addGap(18, 18, 18)
                .addComponent(btnannuler)
                .addGap(18, 18, 18)
                .addComponent(btnquitter)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        tboffrande.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        tboffrande.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tboffrande.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tboffrandeMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tboffrande);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 568, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 8, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 257, Short.MAX_VALUE)
        );

        txtrechercher.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtrechercherKeyReleased(evt);
            }
        });

        cmboff.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Nom Service", "Date" }));

        lbloffrande1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lbloffrande1.setText("Rechercher par :");

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel4.setText("Total Offrande Par monnaie :");

        lbloffHtg.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lbloffHtg.setText("jLabel4");

        lbloffusd.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lbloffusd.setText("jLabel4");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lbloffHtg)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(lbloffusd)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lbloffrande1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(cmboff, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(txtrechercher, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(81, 81, 81)
                        .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(9, 9, 9)
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbloffrande1)
                    .addComponent(cmboff, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtrechercher, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4)
                    .addComponent(lbloffusd)
                    .addComponent(lbloffHtg))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tboffrandeMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tboffrandeMouseClicked
        //vider();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
       indexligne = tboffrande.getSelectedRow();
       model = tboffrande.getModel();
       Numoff= model.getValueAt(indexligne,0).toString();

        try
        {
            String req="Select * from offrande where numero='"+Numoff+"' ";
            st=conn.createStatement();
            rs=st.executeQuery(req);
            if(rs.next())
            {
                txtnomservice.setText(rs.getString("Nom_service"));
                txtmontant.setText(rs.getString("montant"));
                cmbmonaie.setSelectedItem(rs.getString("montant"));
               
                dateoffrande =rs.getString("date_offrande");
                try{
                    dtpdateoff.setDate(formatter.parse(dateoffrande));
                }
                catch (ParseException e){e.printStackTrace();}
                
                btnmodifier.setEnabled(true);
                btnajouter.setEnabled(false);

            }
        }
        catch(SQLException se)
        {
            JOptionPane.showMessageDialog(null,"Erreur Requette"+ se.getMessage());

        }
    }//GEN-LAST:event_tboffrandeMouseClicked

    private void btnquitterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnquitterActionPerformed
       existNO=false;
        this.dispose();
       
    }//GEN-LAST:event_btnquitterActionPerformed

    private void txtmontantKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtmontantKeyTyped
        if(!Character.isDigit(evt.getKeyChar())&& evt.getKeyChar()!='.' )
        {
        evt.consume();
            }
        if(evt.getKeyChar()=='.'&&txtmontant.getText().contains("."))
            {
        evt.consume();
         }
    }//GEN-LAST:event_txtmontantKeyTyped

    private void formInternalFrameClosing(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameClosing
     existNO=false;
    }//GEN-LAST:event_formInternalFrameClosing

    private void formInternalFrameOpened(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameOpened
        listerOffrande();
       calculerOffrande();

    }//GEN-LAST:event_formInternalFrameOpened

    private void txtrechercherKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtrechercherKeyReleased
      listerOffrandePar();
    }//GEN-LAST:event_txtrechercherKeyReleased

    private void btnajouterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnajouterActionPerformed
       if(txtmontant.getText().isEmpty()||txtnomservice.getText().isEmpty())// 
     {
        String mess;
        mess="Vous devez remplir toutes les cases";
        JOptionPane.showMessageDialog(FenOffrande.this, mess);
      }
        else
        {      
             String no="";
             String monnaie=cmbmonaie.getSelectedItem().toString();
             String nom_service=txtnomservice.getText();
             double montan= new Double(txtmontant.getText());
            Date dtoff=dtpdateoff.getDate();
            sqldateOff=new java.sql.Date(dtoff.getTime());
            String dateOff =sqldateOff.toString();
             String repons=ctrlO.ValiderOffrande(no, nom_service,monnaie, montan, dateOff);
            JOptionPane.showMessageDialog(FenOffrande.this, repons);
            listerOffrande();
            calculerOffrande();
            //compterOffrande();
            //calculerdime();
            vider();
                    }
    }//GEN-LAST:event_btnajouterActionPerformed

    private void btnmodifierActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnmodifierActionPerformed
       if(txtmontant.getText().isEmpty()||txtnomservice.getText().isEmpty())// 
     {
        String mess;
        mess="Vous devez remplir toutes les cases";
        JOptionPane.showMessageDialog(FenOffrande.this, mess);
      }
        else
        {      
             String no="";
             String monnaie=cmbmonaie.getSelectedItem().toString();
             String nom_service=txtnomservice.getText();
             double montan= new Double(txtmontant.getText());
            Date dtoff=dtpdateoff.getDate();
            sqldateOff=new java.sql.Date(dtoff.getTime());
            String dateOff =sqldateOff.toString();
             String repons=ctrlO.ModifierOffrande(Numoff, nom_service,monnaie, montan, dateOff);
            JOptionPane.showMessageDialog(FenOffrande.this, repons);
            listerOffrande();
            calculerOffrande();
            //calculerdime();
            //vider();
                    }
    }//GEN-LAST:event_btnmodifierActionPerformed
public void listerOffrande()
  {
        int i=0;
        
         String rela="actif"; 
        String repons="";
        try 
        {
            
            String req="Select numero as Numero,Nom_service as Nom_Service,monnaie as Monnaie,montant as Montant, date_offrande as Date from offrande"; 
           
            pst=conn.prepareStatement(req);
            rs=pst.executeQuery();
            tboffrande.setModel(DbUtils.resultSetToTableModel(rs));
        }
        catch(SQLException se)
            {
                JOptionPane.showMessageDialog(null,"Erreur Requette"+ se.getMessage());
                
            }
       
}
 public void compterOffrand(){
         double total=0.00;
          double tota =0.00;
         try
        {
            String req="Select * from offrande ";
            st=conn.createStatement();
            rs=st.executeQuery(req);
            while(rs.next())
            {
              total=total+Double.parseDouble( rs.getString("montant"));
              //total=total+tota;
            }
            
            //  lbloff.setText(""+total);
        }
        catch(SQLException se)
        {
            JOptionPane.showMessageDialog(null,"Erreur Requette"+ se.getMessage());

        }
    }
public void listerOffrandePar()
  {
        if(cmboff.getSelectedItem().equals("Nom Service"))
        {
        try 
        {
            String rek="Select numero as Numero,Nom_service as Nom_Service,monnaie as Monnaie,montant as Montant, date_offrande as Date from offrande where nom_service like ?"; 
            
            pst=conn.prepareStatement(rek);
            pst.setString(1, txtrechercher.getText() + "%");
            rs = pst.executeQuery();
            tboffrande.setModel(DbUtils.resultSetToTableModel(rs));
             }
        
            catch(SQLException se)
            {
                JOptionPane.showMessageDialog(null,"Erreur Requette"+ se.getMessage());           
            }
        } else if(cmboff.getSelectedItem().equals("Date"))
        {
        try 
        {
            String rek="Select numero as Numero,Nom_service as Nom_Service,monnaie as Monnaie,montant as Montant, date_offrande as Date from offrande where date_offrande like ?"; 
            
            pst=conn.prepareStatement(rek);
            pst.setString(1, txtrechercher.getText() + "%");
            rs = pst.executeQuery();
            tboffrande.setModel(DbUtils.resultSetToTableModel(rs));
             }
        
            catch(SQLException se)
            {
                JOptionPane.showMessageDialog(null,"Erreur Requette"+ se.getMessage());           
            }
        }
}
public void calculerOffrande()
 {
    double usd=0.00;
     double htg=0.00;
     double cad=0.00;
     double eur=0.00;
      try 
        {
            String req="Select * from offrande";
            st=conn.createStatement();
            rs = st.executeQuery(req);
            while(rs.next())
            {
               switch (rs.getString("monnaie")) {
         case "HTG":
            htg=htg+ Double.parseDouble(rs.getString("montant"));
             break;
         case "USD":
            usd=usd+ Double.parseDouble(rs.getString("montant"));
             break;
     }
            }
            lbloffHtg.setText(""+htg+" HTG");
            lbloffusd.setText(""+usd+" USD");
        }
         catch(SQLException se)
            {
                JOptionPane.showMessageDialog(null,"Erreur Requette"+ se.getMessage());   
            } 
   
   
 
 }
 public void vider()
 {
             txtnomservice.setText("");
             txtmontant.setText("");
             dtpdateoff.setDate(null);
             btnmodifier.setEnabled(false);
             btnajouter.setEnabled(false);
 }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnajouter;
    private javax.swing.JButton btnannuler;
    private javax.swing.JButton btnmodifier;
    private javax.swing.JButton btnquitter;
    private javax.swing.JComboBox<String> cmbmonaie;
    private javax.swing.JComboBox cmboff;
    private com.toedter.calendar.JDateChooser dtpdateoff;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lbloffHtg;
    private javax.swing.JLabel lbloffrande1;
    private javax.swing.JLabel lbloffusd;
    private javax.swing.JTable tboffrande;
    private javax.swing.JTextField txtmontant;
    private javax.swing.JTextField txtnomservice;
    private javax.swing.JTextField txtrechercher;
    // End of variables declaration//GEN-END:variables
}
