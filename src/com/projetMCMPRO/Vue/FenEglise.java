/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projetMCMPRO.Vue;

import com.projetMCMPRO.Controleur.ControleurEglise;
import com.projetMCMPRO.Dal.mysqlconnect;
import java.awt.event.KeyEvent;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;

/**
 *
 * @author INGSAMUEL
 */
public class FenEglise extends javax.swing.JInternalFrame {

   public static Connection conn;
    public static Statement st;
    
    public String codEg;
    public ResultSet rs;
     public static  boolean existE;
    public static ControleurEglise ctrle=new ControleurEglise();
    public FenEglise() {
        initComponents();
        conn=mysqlconnect.ConnectDB();
        existE=true;
    }

   public void AjouterEglise()
   {
    char c= this.txtnomEgl.getText().toUpperCase().charAt(0);
    char o= this.txtnomEgl.getText().toUpperCase().charAt(1);
    char d= this.txtnomEgl.getText().toUpperCase().charAt(2);   
   String codExp="ASS-"+c+""+0+""+d+"-001";
   String nomegl=txtnomEgl.getText();
   String nompast =txtnompast.getText();
   String adres = txtadressExp.getText();
   String tel1=txttel1.getText();
   String tel2=txttel2.getText();    
   String repons = ctrle.ValiderEglise(codExp, nomegl, nompast, adres, tel1, tel2);
   JOptionPane.showMessageDialog(FenEglise.this,repons);
   }
   public void ModifierEglise()
   {
   String nomegl=txtnomEgl.getText();
   String adres = txtadressExp.getText();
   String nompast=txtnompast.getText();
   String tel1=txttel1.getText();
   String tel2=txttel2.getText();    
   String repons = ctrle.ModifierEglise(codEg, nomegl,nompast, adres, tel1, tel2);
   JOptionPane.showMessageDialog(FenEglise.this,repons);
   }
   public void afficherEglise()
   {
   try 
        {
            String req="Select * from eglise"; 
           // con=DriverManager.getConnection(chainConnection,user,password);
            st=conn.createStatement();
            rs = st.executeQuery(req);
            if(rs.next())
            {      
            codEg=rs.getString("codeEglise");
            txtnomEgl.setText(rs.getString("nomEglise"));
            txtnompast.setText(rs.getString("nomPasteur"));
            txtadressExp.setText(rs.getString("adresse"));
            txttel1.setText(rs.getString("telephone1"));
            txttel2.setText(rs.getString("telephone2"));
            }
        }
         catch(SQLException se)
            {
                JOptionPane.showMessageDialog(null,"Erreur Requette"+ se.getMessage());   
            } 
   
   }
   public void EnregistrerEglise()
{;
 try 
        {
            String req="Select * from eglise "; 
            //con=DriverManager.getConnection(chainConnection,user,password);
            st=conn.createStatement();
            rs=st.executeQuery(req);
            if(rs.next())
            {  
               
            ModifierEglise();
            }
            else
            {
             AjouterEglise();
            }
        }
         catch(SQLException se)
            {
                JOptionPane.showMessageDialog(null,"Erreur Requette"+ se.getMessage());   
            } 
}
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtnomEgl = new javax.swing.JTextField();
        txtadressExp = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        txttel1 = new javax.swing.JFormattedTextField();
        txttel2 = new javax.swing.JFormattedTextField();
        txtnompast = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        btnvalider = new javax.swing.JButton();
        btnquitter = new javax.swing.JButton();

        setClosable(true);
        addInternalFrameListener(new javax.swing.event.InternalFrameListener() {
            public void internalFrameActivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosed(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosing(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameClosing(evt);
            }
            public void internalFrameDeactivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeiconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameIconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameOpened(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameOpened(evt);
            }
        });

        jPanel1.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jLabel1.setText("Informations de l'eglise");

        jLabel2.setText("Nom Eglise :");

        txtnomEgl.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtnomEglActionPerformed(evt);
            }
        });
        txtnomEgl.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtnomEglKeyTyped(evt);
            }
        });

        jLabel3.setText("Adresse :");

        jLabel4.setText("Telephone 1 :");

        jLabel5.setText("Telephone 2 :");

        try {
            txttel1.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("(509)####-####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txttel1.setToolTipText("");

        try {
            txttel2.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("(509)####-####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txttel2.setToolTipText("");

        jLabel6.setText("Diriger Par:");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jLabel1)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGap(56, 56, 56)
                            .addComponent(jLabel3)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(txtadressExp))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGap(84, 84, 84)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addComponent(jLabel5)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(txttel2, javax.swing.GroupLayout.PREFERRED_SIZE, 176, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addComponent(jLabel4)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(txttel1, javax.swing.GroupLayout.PREFERRED_SIZE, 176, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                        .addGap(50, 50, 50)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtnomEgl, javax.swing.GroupLayout.PREFERRED_SIZE, 224, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel6)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtnompast, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap(23, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtnomEgl, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(txtnompast, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 23, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtadressExp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txttel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(txttel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jPanel2.setBackground(new java.awt.Color(153, 153, 255));

        btnvalider.setText("Valider");
        btnvalider.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnvaliderActionPerformed(evt);
            }
        });

        btnquitter.setText("Quitter");
        btnquitter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnquitterActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnvalider, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnquitter, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnvalider)
                    .addComponent(btnquitter))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(13, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnvaliderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnvaliderActionPerformed
        if(txtnomEgl.getText().isEmpty()||txtadressExp.getText().isEmpty()||txttel1.getText().isEmpty())
        {
            JOptionPane.showMessageDialog(FenEglise.this,"Champs Vide");
        }
        else
        {
            EnregistrerEglise();
        }
    }//GEN-LAST:event_btnvaliderActionPerformed

    private void btnquitterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnquitterActionPerformed
        existE=false;
        this.dispose();

    }//GEN-LAST:event_btnquitterActionPerformed

    private void formInternalFrameOpened(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameOpened
      // conn= mysqlconnect.ConnectDB();
        afficherEglise();
    }//GEN-LAST:event_formInternalFrameOpened

    private void formInternalFrameClosing(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameClosing
      existE=false;
       this.dispose();
    }//GEN-LAST:event_formInternalFrameClosing

    private void txtnomEglActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtnomEglActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtnomEglActionPerformed

    private void txtnomEglKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtnomEglKeyTyped
 char keyChar = evt.getKeyChar();
if (Character.isLowerCase(keyChar)) {
    evt.setKeyChar(Character.toUpperCase(keyChar));
}
char car= evt.getKeyChar();
if ((Character.isDigit(car)|| (car==KeyEvent.VK_BACK_SPACE) ||
car==KeyEvent.VK_DELETE) )
{
evt.consume();
}
    }//GEN-LAST:event_txtnomEglKeyTyped


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnquitter;
    private javax.swing.JButton btnvalider;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JTextField txtadressExp;
    private javax.swing.JTextField txtnomEgl;
    private javax.swing.JTextField txtnompast;
    private javax.swing.JFormattedTextField txttel1;
    private javax.swing.JFormattedTextField txttel2;
    // End of variables declaration//GEN-END:variables
}
