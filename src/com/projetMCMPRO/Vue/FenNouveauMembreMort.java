
package com.projetMCMPRO.Vue;

import javax.swing.JOptionPane;

//import com.projetSystra.Controleur.ControleurFournisseur;
import com.projetMCMPRO.Controleur.ControleurMembreMort;
import com.projetMCMPRO.Dal.mysqlconnect;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.MessageFormat;
import java.util.Date;
import java.util.Locale;
//import java.lang.NullPointerException;
import java.text.ParseException; 
import java.text.SimpleDateFormat; 
//import javax.swing.JComboBox
import javax.swing.JTable;
import javax.swing.table.TableModel;
import net.proteanit.sql.DbUtils;

public class FenNouveauMembreMort extends javax.swing.JInternalFrame {
    public static Connection conn;
    public static Statement st;
    private static final long serialVersionUID = 1L;
    public ResultSet rs;
    public PreparedStatement pst = null;
    public java.sql.Date sqldateFune, sqldateDeces,datCon;
    //public Date datCon;
    public String datefune,datemort; 
   //public  ControleurMembre ctrl =new ControleurMembre();
    public  ControleurMembreMort ctrlM =new ControleurMembreMort();
    //Methode qui nettoie les combo a chargement de la fenetre
   
    public String clic_table;
    public String codeMemb, co;
    public String imel,reference;
    public static  boolean existNM;

    public FenNouveauMembreMort() {
    conn=mysqlconnect.ConnectDB();
    existNM=true;
    initComponents();
    textEnabledFalse();
   // btnValider.setEnabled(false);
    
    }
     //creation de l'objet ctrl de la classe controleurMembre
   
   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btncin_nif = new javax.swing.ButtonGroup();
        jPanel2 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        txtcodeMembre = new javax.swing.JTextField();
        btnOkMembre = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        txtcausemort = new javax.swing.JTextField();
        dtpfuneraile = new com.toedter.calendar.JDateChooser();
        jLabel21 = new javax.swing.JLabel();
        dtpdeces = new com.toedter.calendar.JDateChooser();
        btnenregistrer = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbMembre = new javax.swing.JTable();
        jLabel6 = new javax.swing.JLabel();
        cmbmemb = new javax.swing.JComboBox();
        jLabel7 = new javax.swing.JLabel();
        txtrechercherpar = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        lblnbremembre = new javax.swing.JLabel();
        btnImprimer = new javax.swing.JButton();
        btnannuler1 = new javax.swing.JButton();
        btnAnnuler = new javax.swing.JButton();
        btnmodifier = new javax.swing.JButton();

        setClosable(true);
        setTitle("Nouveau Membre Mort");
        addInternalFrameListener(new javax.swing.event.InternalFrameListener() {
            public void internalFrameActivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosed(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosing(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameClosing(evt);
            }
            public void internalFrameDeactivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeiconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameIconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameOpened(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameOpened(evt);
            }
        });

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel2.setText("Code Membre :");

        btnOkMembre.setText("Verifier code");
        btnOkMembre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOkMembreActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtcodeMembre, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnOkMembre, javax.swing.GroupLayout.DEFAULT_SIZE, 136, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(txtcodeMembre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(jLabel2)
                .addComponent(btnOkMembre))
        );

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel12.setText("Date de decès");

        jLabel16.setText("Cause de la mort");

        txtcausemort.setEnabled(false);

        dtpfuneraile.setEnabled(false);

        jLabel21.setText("Date de Funeraille");

        dtpdeces.setEnabled(false);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel12)
                        .addGap(22, 22, 22)
                        .addComponent(dtpdeces, javax.swing.GroupLayout.DEFAULT_SIZE, 242, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel16)
                            .addComponent(jLabel21))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(dtpfuneraile, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtcausemort))))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel12)
                    .addComponent(dtpdeces, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtcausemort, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel16))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(dtpfuneraile, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(jLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(32, Short.MAX_VALUE))
        );

        btnenregistrer.setText("Enregistrer");
        btnenregistrer.setEnabled(false);
        btnenregistrer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnenregistrerActionPerformed(evt);
            }
        });

        tbMembre.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tbMembre.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        tbMembre.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        tbMembre.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbMembreMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tbMembre);

        jLabel6.setText("Lister Par:");

        cmbmemb.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Code Membre", "Nom Membre", "Fonction", "Profession" }));

        jLabel7.setText("Rechercher");

        txtrechercherpar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtrechercherparKeyReleased(evt);
            }
        });

        jLabel15.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel15.setText("L'Assemblée de la ruelle Jardine  a");

        lblnbremembre.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lblnbremembre.setText("nbrmembre");

        btnImprimer.setText("Imprimer");
        btnImprimer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnImprimerActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(cmbmemb, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(27, 27, 27)
                .addComponent(jLabel7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtrechercherpar, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 518, Short.MAX_VALUE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 221, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblnbremembre, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnImprimer)))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(cmbmemb, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtrechercherpar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblnbremembre)
                    .addComponent(jLabel15)
                    .addComponent(btnImprimer))
                .addGap(6, 6, 6))
        );

        btnannuler1.setText("Quitter");
        btnannuler1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnannuler1ActionPerformed(evt);
            }
        });

        btnAnnuler.setText("Annuler");
        btnAnnuler.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAnnulerActionPerformed(evt);
            }
        });

        btnmodifier.setText("Modifier");
        btnmodifier.setEnabled(false);
        btnmodifier.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnmodifierActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnenregistrer)
                        .addGap(18, 18, 18)
                        .addComponent(btnmodifier, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(18, 18, 18)
                        .addComponent(btnAnnuler)
                        .addGap(18, 18, 18)
                        .addComponent(btnannuler1)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(43, 43, 43)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnenregistrer)
                    .addComponent(btnAnnuler)
                    .addComponent(btnannuler1)
                    .addComponent(btnmodifier))
                .addContainerGap(15, Short.MAX_VALUE))
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
//Rechercher si le membre est deja mort
    public void RecherchMembreMort() 
{
    int compt=1;
    String codM=txtcodeMembre.getText();
   try{
     String reket="Select * from membremort where CodeMembre='"+codM+"'";
     st=conn.createStatement();
     rs=st.executeQuery(reket); 
      if(rs.next())
      {
          JOptionPane.showMessageDialog(null, "Le code membre saisi est deja dans la liste des deces");  
      }
     else{ textEnabbledTrue();}
     }
     catch(SQLException se)
    {
      System.out.println("Probleme Connexion, raison:"+se.getMessage());       
    }
 }
 //Rechercher si le membre existe 
  public  void RechercheMembre() {
     String codM=txtcodeMembre.getText();
  try{
     String reket="Select * from membre where CodeMembre='"+codM+"'";
     st=conn.createStatement();
     rs=st.executeQuery(reket);
     
     if(rs.next())
     {
         RecherchMembreMort();
         btnenregistrer.setEnabled(true);
         btnmodifier.setEnabled(false);
     } 
     else{JOptionPane.showMessageDialog(null, "Le code membre saisi n'existe pas");}
     }
     catch(SQLException se)
    {
      System.out.println("Probleme Connexion, raison:"+se.getMessage());       
    }
 }  
   // Methode de recherche a partir du code
    private void btnOkMembreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOkMembreActionPerformed
      if(txtcodeMembre.getText().isEmpty())
      {
      JOptionPane.showMessageDialog(FenNouveauMembreMort.this,"Champs Vide");
      }
       else
          {
         RechercheMembre();
          }  

    }//GEN-LAST:event_btnOkMembreActionPerformed
    //Appel a la methode lister membre lorsque la fenetre est ouverte
    private void formInternalFrameOpened(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameOpened
     listerMembreMort();
     compterMembreMort();    
    }//GEN-LAST:event_formInternalFrameOpened
     //Methode pour compter les membre actif
    public void compterMembreMort(){
         int compte = tbMembre.getRowCount();
         lblnbremembre.setText(""  +compte+"  Membre(s) mort(s)");  
    }
    //Appel a ces deux methodes lorsqu'on cliique sur le bouton annuler
    private void btnannuler1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnannuler1ActionPerformed
      existNM=false;
      this.dispose();
    }//GEN-LAST:event_btnannuler1ActionPerformed
    
    //Appel a la methode rechercherMembrePar
    private void txtrechercherparKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtrechercherparKeyReleased
     rehercherMembrePar();       
    }//GEN-LAST:event_txtrechercherparKeyReleased
  
    //Appel a la methode vider
    private void btnAnnulerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAnnulerActionPerformed
        vider();
    }//GEN-LAST:event_btnAnnulerActionPerformed
  
    //Implementation de la methode modifier
    private void btnenregistrerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnenregistrerActionPerformed
   
     if(txtcausemort.getText().isEmpty())
     {
       String mess;
       mess="Vous devez remplir toutes les cases";
       JOptionPane.showMessageDialog(FenNouveauMembreMort.this, mess);
      }
      else
      { 
        codeMemb=txtcodeMembre.getText(); 
         String cod=txtcodeMembre.getText();      
         Date dateDeces=dtpdeces .getDate();
         sqldateDeces=new java.sql.Date(dateDeces.getTime());
         String datedeces =sqldateDeces.toString();
         String cause=txtcausemort.getText();
         Date dateFune=dtpfuneraile .getDate();
         sqldateFune=new java.sql.Date(dateFune.getTime());
         String datefun =sqldateFune.toString();
         String reponse=ctrlM.ValiderMembreMort(cod,datedeces,cause,datefun);
         UpdateMembre();
         JOptionPane.showMessageDialog(FenNouveauMembreMort.this,reponse); 
         listerMembreMort();
         compterMembreMort();
         vider();
          
      }
    }//GEN-LAST:event_btnenregistrerActionPerformed
public void UpdateMembre() 
{    
        String rela="Mort";
        String reket="Update membre set relation='"+rela+"' where codeMembre='"+codeMemb+"'";
        int verif = 0;
        String  repons="";
        try
        {
        //con=DriverManager.getConnection(chainConnection, user, password);    
        st=conn.createStatement();
        verif = st.executeUpdate(reket);
        st.close();
        if (verif!=0)
        {
        repons="Modification Reussie";}
        }  
        catch(SQLException cn2)
        {
        repons="Probleme Connexion, pou kisa:"+cn2.getMessage(); 
        }
 } 
    //Appel a la methode ImprimerTable
    private void btnImprimerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnImprimerActionPerformed
     ImprimerTable();        // TODO add your handling code here:
    }//GEN-LAST:event_btnImprimerActionPerformed

   
    private void tbMembreMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbMembreMouseClicked
        int index = tbMembre.getSelectedRow();
        TableModel mod = tbMembre.getModel();
        txtcodeMembre.setText(mod.getValueAt(index, 0).toString());  
        rechercheMembreMort(); 
        textEnabbledTrue();
        btnmodifier.setEnabled(true);
        btnenregistrer.setEnabled(false);
    }//GEN-LAST:event_tbMembreMouseClicked

    //Appel a cette methode lors de la fermeture de la forme.
    private void formInternalFrameClosing(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameClosing
        existNM=false;
    }//GEN-LAST:event_formInternalFrameClosing

    private void btnmodifierActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnmodifierActionPerformed
       if(txtcausemort.getText().isEmpty())
     {
       String mess;
       mess="Vous devez remplir toutes les cases";
       JOptionPane.showMessageDialog(FenNouveauMembreMort.this, mess);
      }
      else
      { 
        codeMemb=txtcodeMembre.getText(); 
         String cod=txtcodeMembre.getText();      
         Date dateDeces=dtpdeces .getDate();
         sqldateDeces=new java.sql.Date(dateDeces.getTime());
         String datedeces =sqldateDeces.toString();
         String cause=txtcausemort.getText();
         Date dateFune=dtpfuneraile .getDate();
         sqldateFune=new java.sql.Date(dateFune.getTime());
         String datefun =sqldateFune.toString();
         String reponse=ctrlM.ModifierMembreMort(cod,datedeces,cause,datefun);
         JOptionPane.showMessageDialog(FenNouveauMembreMort.this,reponse); 
         listerMembreMort();
         compterMembreMort();
         vider();
          
      }  
    }//GEN-LAST:event_btnmodifierActionPerformed
  //Methode permettant de controlerIIetm autre
    public void controlerIetm(){

    }    
   
//Implementation de la methode RechercheMembre mort
    public void rechercheMembreMort()
    {
        String codemembre=txtcodeMembre.getText();
        txtcodeMembre.requestFocus();//positionne le focus sur le champ
         if(codemembre.length()==0)
        {
        JOptionPane.showMessageDialog(FenNouveauMembreMort.this, "Champ code vide!");  
         return;
        }
         boolean reponse=ctrlM.RechercheMembreMort(txtcodeMembre.getText());
         if(!reponse)
         { 
           JOptionPane.showMessageDialog(FenNouveauMembreMort.this, " Ce code n'existe pas", "Error", JOptionPane.ERROR_MESSAGE );  
           }
             
           else{         
              textEnabbledTrue(); 
              
              SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
               datemort =ctrlM.PrendDateDeces();
               try{ 
                  dtpdeces.setDate(formatter.parse(datemort));
                  }
                   catch (ParseException e){e.printStackTrace();}
                
              txtcausemort.setText(ctrlM.PrendCause());
                datefune =ctrlM.PrendDateFune();
               try{ 
                  dtpfuneraile.setDate(formatter.parse(datefune));
                  }
                   catch (ParseException e){e.printStackTrace();}
              btnenregistrer.setEnabled(false);
              btnmodifier.setEnabled(true);
        }
              
       }
               
   //Implementation de la methode listerMembre
    String rela="mort";
    public void listerMembreMort()
    {
        int i=0;  
        String repons="";
        try 
           {
            String req="Select membremort.codeMembre as Code,nomMemb as Nom ,prenomMemb as Prenom, date_deces, Cause, date_funeraille from membremort inner join membre on membre.codeMembre =membremort.Codemembre  where relation='"+rela+"'"; 
            
            pst=conn.prepareStatement(req);
            rs=pst.executeQuery();
            tbMembre.setModel(DbUtils.resultSetToTableModel(rs));
            }
             catch(SQLException se)
            {
                JOptionPane.showMessageDialog(null,"Erreur Requette"+ se.getMessage());   
            }
       }
    //Implementation de la methode rechercherMembrePar
     public void rehercherMembrePar()
     {
       if (cmbmemb.getSelectedItem().equals("Code Membre"))
       {
         String repons="";
        try 
           {
            String rek="Select codeMembre as Code,titre as Titre, nomMemb as Nom,prenomMemb as Prenoom,telephone as Telephone,fonction1 as Fonction,profession1 as Profession from membremort where codeMembre like ?"; 
            
            pst=conn.prepareStatement(rek);
            pst.setString(1, txtrechercherpar.getText() + "%");
            rs = pst.executeQuery();
            tbMembre.setModel(DbUtils.resultSetToTableModel(rs));
             }
             catch(SQLException se)
             {
                JOptionPane.showMessageDialog(null,"Erreur Requette"+ se.getMessage());
                
              }
           }
            else if(cmbmemb.getSelectedItem().equals("Fonction"))
            {
            String repons="";
            try 
              {
            String rek="Select codeMembre as Code,titre as Titre,omMemb as Nom,prenomMemb as Prenom,telephone as Telephone,fonction1 as Fonction,profession1 as Profession from membremort where fonction1 like ?"; 
            
            pst=conn.prepareStatement(rek);
            pst.setString(1, txtrechercherpar.getText() + "%");
            rs = pst.executeQuery();
            tbMembre.setModel(DbUtils.resultSetToTableModel(rs));
             }
            catch(SQLException se)
            {
                JOptionPane.showMessageDialog(null,"Erreur Requette"+ se.getMessage());    
            }   
         }
           else if(cmbmemb.getSelectedItem().equals("Nom Membre"))
           {
              String repons="";
              try 
              {
              String rek="Select codeMembre as Code,titre as Titre,nomMemb as Nom,prenomMemb as Prenom,telephone as Telephone,fonction1 as Fonction,profession1 as Profession from Membremort where nomMemb like ?"; 
              
              pst=conn.prepareStatement(rek);
              pst.setString(1, txtrechercherpar.getText() + "%");
              rs = pst.executeQuery();
              tbMembre.setModel(DbUtils.resultSetToTableModel(rs));
              }
              catch(SQLException se)
              {
                JOptionPane.showMessageDialog(null,"Erreur Requette"+ se.getMessage());   
              }    
             }
       }
 
     
     //Implementation de la methode vider les donnees dans les champs
      public void effacerDonnees(){
      
       txtcodeMembre.setText("");
       dtpdeces.setDate(null);
       dtpfuneraile.setDate(null);
       txtcausemort.setText("");
       }
      //Implementation de la methode vider
       public void vider()
       {
        txtcodeMembre.setText("");
       
        dtpdeces.setDate(null);
        
        dtpfuneraile.setDate(null);
       
        txtcausemort.setText("");  
        btnenregistrer.setEnabled(false);
        }
       //Implementation de la methode textEnabbledTrue
        private void textEnabbledTrue()
        {
         dtpfuneraile.setEnabled(true);
         dtpdeces.setEnabled(true);
         txtcausemort.setEnabled(true);
        // btnenregistrer.setEnabled(true);
         }
         private void textEnabledFalse()
         {
         // txtcodeMembre.setEnabled(false);
          
          dtpfuneraile.setEnabled(false);
          dtpdeces.setEnabled(false);
          txtcausemort.setEnabled(false);
         }
      //Implementation de la methode ImprimerTable
      public void ImprimerTable()
      {
       MessageFormat header = new MessageFormat("Liste des Membres");
       MessageFormat footer= new MessageFormat("Page {0,number,interger}");
       MessageFormat details=new MessageFormat("Yblad", Locale.FRENCH);
       try
       {
        tbMembre.print(JTable.PrintMode.FIT_WIDTH, header,footer);
        // tblisteachat.print(JTable.PrintMode.FIT_WIDTH, header, footer);
        } 
       catch (java.awt.print.PrinterException se)
       {
       System.err.format("Impossible d'imprimer:", se.getMessage());
       }
}
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAnnuler;
    private javax.swing.JButton btnImprimer;
    private javax.swing.JButton btnOkMembre;
    private javax.swing.JButton btnannuler1;
    private javax.swing.ButtonGroup btncin_nif;
    private javax.swing.JButton btnenregistrer;
    private javax.swing.JButton btnmodifier;
    private javax.swing.JComboBox cmbmemb;
    private com.toedter.calendar.JDateChooser dtpdeces;
    private com.toedter.calendar.JDateChooser dtpfuneraile;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblnbremembre;
    private javax.swing.JTable tbMembre;
    private javax.swing.JTextField txtcausemort;
    private javax.swing.JTextField txtcodeMembre;
    private javax.swing.JTextField txtrechercherpar;
    // End of variables declaration//GEN-END:variables
}
