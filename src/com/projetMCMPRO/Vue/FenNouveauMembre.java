/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projetMCMPRO.Vue;

import javax.swing.JOptionPane;

//import com.projetSystra.Controleur.ControleurFournisseur;
import com.projetMCMPRO.Controleur.ControleurMembre;
import com.projetMCMPRO.Controleur.ControleurMembreMort;
import com.projetMCMPRO.Dal.mysqlconnect;
//import java.awt.event.*;
import java.awt.event.*;
//import javax.swing.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.MessageFormat;
import java.util.Date;
import java.util.Locale;
//import java.lang.NullPointerException;
import java.text.ParseException; 
import java.text.SimpleDateFormat; 
import java.util.logging.Level;
import java.util.logging.Logger;
//import javax.swing.JComboBox
import javax.swing.JTable;
import javax.swing.table.TableModel;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.MaskFormatter;
import net.proteanit.sql.DbUtils;

public class FenNouveauMembre extends javax.swing.JInternalFrame {
    public static Connection conn;
    public static Statement st;
    private static final long serialVersionUID = 1L;
    public ResultSet rs;
    public PreparedStatement pst = null;
    public java.sql.Date sqldateNais, sqldateDeces,datCon;
    public String datenaissans,datebapteme; 
    public  ControleurMembre ctrl =new ControleurMembre();
    public  ControleurMembreMort ctrlM =new ControleurMembreMort();
    
    //Methode qui nettoie les combo a chargement de la fenetre
    private void NettoyerComboBox(){
    cmbtitre.setSelectedItem(null);
    cmbcommune.setSelectedItem(null);
    cmbstatut.setSelectedItem(null);
    cmbprofe1.setSelectedItem(null);
    cmbfonction1.setSelectedItem(null);
    cmbrelation.setSelectedItem(null);
    }
    public String clic_table;
    public String codeMemb, co;
    public static  boolean existNM;

    public FenNouveauMembre() {
    existNM=true;
    initComponents();
    conn=mysqlconnect.ConnectDB();
    NettoyerComboBox();
   // btnValider.setEnabled(false);
    textEnabbledTrue();
    }
     //creation de l'objet ctrl de la classe controleurMembre
   
   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btncin_nif = new javax.swing.ButtonGroup();
        jPanel2 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        txtcodeMembre = new javax.swing.JTextField();
        btnOkMembre = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtnomMembre = new javax.swing.JTextField();
        txtprenomMemb = new javax.swing.JTextField();
        txtadresse = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        txtCINIF = new javax.swing.JFormattedTextField();
        txttelephone = new javax.swing.JFormattedTextField();
        rbt_cin = new javax.swing.JRadioButton();
        rbt_nif = new javax.swing.JRadioButton();
        cmbtitre = new javax.swing.JComboBox<String>();
        jLabel4 = new javax.swing.JLabel();
        dtpdatenais = new com.toedter.calendar.JDateChooser();
        jLabel11 = new javax.swing.JLabel();
        cmbstatut = new javax.swing.JComboBox<String>();
        jLabel20 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        txtmail = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        cmbprofe1 = new javax.swing.JComboBox();
        jLabel9 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        txtrefere = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        cmbfonction1 = new javax.swing.JComboBox<String>();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        cmbrelation = new javax.swing.JComboBox<String>();
        cmbprofe2 = new javax.swing.JTextField();
        cmbfonction2 = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        cmbcommune = new javax.swing.JComboBox<String>();
        dtpdatebap = new com.toedter.calendar.JDateChooser();
        btnValider = new javax.swing.JButton();
        btnmodifier = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbMembre = new javax.swing.JTable();
        jLabel6 = new javax.swing.JLabel();
        cmbmemb = new javax.swing.JComboBox();
        jLabel7 = new javax.swing.JLabel();
        txtrechercherpar = new javax.swing.JTextField();
        btnannuler1 = new javax.swing.JButton();
        btnAnnuler = new javax.swing.JButton();
        btnImprimer = new javax.swing.JButton();
        jLabel15 = new javax.swing.JLabel();
        lblnbremembre = new javax.swing.JLabel();

        setClosable(true);
        setTitle("Nouveau Membre");
        addInternalFrameListener(new javax.swing.event.InternalFrameListener() {
            public void internalFrameActivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosed(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosing(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameClosing(evt);
            }
            public void internalFrameDeactivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeiconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameIconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameOpened(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameOpened(evt);
            }
        });

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel2.setText("Code Membre :");

        txtcodeMembre.setEnabled(false);

        btnOkMembre.setText("Verifier code");
        btnOkMembre.setEnabled(false);
        btnOkMembre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOkMembreActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtcodeMembre, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnOkMembre, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(txtcodeMembre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(jLabel2)
                .addComponent(btnOkMembre))
        );

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel1.setText("Nom :");

        txtnomMembre.setEnabled(false);
        txtnomMembre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtnomMembreKeyTyped(evt);
            }
        });

        txtprenomMemb.setEnabled(false);
        txtprenomMemb.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtprenomMembKeyTyped(evt);
            }
        });

        txtadresse.setToolTipText("Format adresse: (No, Impasse/Rue/Avenue/Route, Zone, Commune)");
        txtadresse.setEnabled(false);

        jLabel3.setText("Prenom:");

        jLabel5.setText("Adresse :");

        jLabel8.setText("Telephone :");

        txtCINIF.setEnabled(false);

        try {
            txttelephone.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("(509)####-####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txttelephone.setToolTipText("Format de telephone:(509)-####-####");
        txttelephone.setEnabled(false);

        btncin_nif.add(rbt_cin);
        rbt_cin.setText("CIN");
        rbt_cin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbt_cinActionPerformed(evt);
            }
        });

        btncin_nif.add(rbt_nif);
        rbt_nif.setText("NIF");
        rbt_nif.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbt_nifActionPerformed(evt);
            }
        });

        cmbtitre.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Frere", "Soeur" }));
        cmbtitre.setEnabled(false);

        jLabel4.setText("Titre:");

        dtpdatenais.setDateFormatString("dd-MM-yyyy");
        dtpdatenais.setEnabled(false);

        jLabel11.setText("Date de naissance:");

        cmbstatut.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Celibataire", "Marie(e)", "Veuf(ve)", "Divorcé(e)" }));
        cmbstatut.setEnabled(false);

        jLabel20.setText("Statut matrimonial:");

        jLabel12.setText("E-Mail :");

        txtmail.setEnabled(false);
        txtmail.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtmailKeyTyped(evt);
            }
        });

        jLabel13.setText("Profession:");

        cmbprofe1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Aucune", "Agronome", "Avocat", "Cameraman", "Comptable", "Charpente", "Chaufeur", "Gestionnaire", "Ebeniste", "Electricien", "Ing. Civil", "Infirmiere", "Ing. Informaticien", "Mecanicien", "Medecin", "Tailleur", "Autre" }));
        cmbprofe1.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbprofe1ItemStateChanged(evt);
            }
        });

        jLabel9.setText("Profession:");

        jLabel16.setText("Reference:");

        txtrefere.setEnabled(false);

        jLabel14.setText("Date de bapteme:");

        jLabel17.setText("Fonction:");

        cmbfonction1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Aucune", "Administrateur", "Conducteur de chants", "Consiège", "Diacre", "huissier", "Musicien", "Pasteur", "Predicateur", "Autre" }));
        cmbfonction1.setEnabled(false);
        cmbfonction1.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbfonction1ItemStateChanged(evt);
            }
        });

        jLabel18.setText("Fonction:");

        jLabel19.setText("Statut membre");

        cmbrelation.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Actif", "Inactif", "Livré" }));
        cmbrelation.setEnabled(false);

        cmbprofe2.setEnabled(false);

        cmbfonction2.setEnabled(false);

        jLabel10.setText("Commune:");

        cmbcommune.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Anase-à-Galet", "Arcahaie", "Cabaret", "Carrefour", "Cite soleil", "Cornillon/Grand Bois", "Croix-des-Bouquet", "Delmas", "Fonds-Verrettes", "Ganthier", "Grand-Goave", "Gressier", "Kenscoff", "Leogane", "Petion-ville", "Petit-Goave", "Pointe-à-raquette", "Port-au-prince", "Tabarre", "Thomazeau" }));

        dtpdatebap.setDateFormatString("dd-MM-yyyy");
        dtpdatebap.setEnabled(false);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel19)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cmbrelation, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtnomMembre))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtprenomMemb))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cmbtitre, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jLabel20, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cmbstatut, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(dtpdatenais, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(rbt_cin, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(rbt_nif, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtCINIF, javax.swing.GroupLayout.PREFERRED_SIZE, 216, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel18)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cmbfonction2))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel12)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtmail))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel8)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txttelephone))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel10)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cmbcommune, javax.swing.GroupLayout.PREFERRED_SIZE, 262, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGap(1, 1, 1)
                                .addComponent(txtadresse, javax.swing.GroupLayout.PREFERRED_SIZE, 256, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cmbprofe2)
                            .addComponent(cmbprofe1, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel16)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtrefere))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel14)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(dtpdatebap, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel17)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cmbfonction1, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtnomMembre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtprenomMemb, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addGap(7, 7, 7)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4)
                    .addComponent(cmbtitre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(dtpdatenais, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel11))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel20)
                    .addComponent(cmbstatut, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtCINIF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(rbt_nif)
                    .addComponent(rbt_cin))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel10)
                    .addComponent(cmbcommune, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(txtadresse, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(txttelephone, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(txtmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13)
                    .addComponent(cmbprofe1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(cmbprofe2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel16)
                    .addComponent(txtrefere, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel14)
                    .addComponent(dtpdatebap, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbfonction1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel17))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel18)
                    .addComponent(cmbfonction2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cmbrelation, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        btnValider.setText("Valider");
        btnValider.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnValiderActionPerformed(evt);
            }
        });

        btnmodifier.setText("Modifier");
        btnmodifier.setEnabled(false);
        btnmodifier.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnmodifierActionPerformed(evt);
            }
        });

        tbMembre.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tbMembre.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbMembreMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tbMembre);

        jLabel6.setText("Lister Par:");

        cmbmemb.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Code Membre", "Nom Membre", "Fonction", "Profession" }));

        jLabel7.setText("Rechercher");

        txtrechercherpar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtrechercherparKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(75, 75, 75)
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cmbmemb, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtrechercherpar, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(228, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addComponent(jScrollPane1)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(cmbmemb, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtrechercherpar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 443, Short.MAX_VALUE))
        );

        btnannuler1.setText("Quitter");
        btnannuler1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnannuler1ActionPerformed(evt);
            }
        });

        btnAnnuler.setText("Annuler");
        btnAnnuler.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAnnulerActionPerformed(evt);
            }
        });

        btnImprimer.setText("Imprimer");
        btnImprimer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnImprimerActionPerformed(evt);
            }
        });

        jLabel15.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel15.setText("L'Assemblée de la ruelle Jardine  a");

        lblnbremembre.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lblnbremembre.setText("nbrmembre");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnValider, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnmodifier)
                        .addGap(18, 18, 18)
                        .addComponent(btnAnnuler, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(10, 10, 10)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnImprimer)
                        .addGap(18, 18, 18)
                        .addComponent(btnannuler1)
                        .addGap(54, 54, 54)
                        .addComponent(jLabel15)
                        .addGap(33, 33, 33)
                        .addComponent(lblnbremembre, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnValider)
                            .addComponent(btnmodifier)
                            .addComponent(btnAnnuler)))
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnImprimer)
                            .addComponent(btnannuler1)))
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel15)
                            .addComponent(lblnbremembre))))
                .addContainerGap(19, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    
   //Methode d'enregistrement d'un membre
    private void btnValiderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnValiderActionPerformed
    String titres=(String)cmbtitre.getSelectedItem();
    cmbtitre.requestFocus();//positionne le focus sur le champ
    if(titres==null){
     JOptionPane.showMessageDialog(null, "Champ titre du membre vide! S'il vous plait selectionnez un");  
    return;
    }
     String statu=(String)cmbstatut.getSelectedItem();
     cmbstatut.requestFocus();//positionne le focus sur le champ
    if(statu==null){
     JOptionPane.showMessageDialog(null, "Champ statut du membre vide! S'il vous plait selectionnez un");  
     return;
     }  
    
     if(txtnomMembre.getText().isEmpty()||txtprenomMemb.getText().isEmpty()||txtCINIF.getText().isEmpty()||txtadresse.getText().isEmpty()||txttelephone.getText().isEmpty()|| txtmail.getText().isEmpty()||txtrefere .getText().isEmpty()||cmbprofe2.getText().isEmpty()||cmbfonction2.getText().isEmpty())// 
     {
       String mess;
       mess="Vous devez remplir toutes les cases";
       JOptionPane.showMessageDialog(FenNouveauMembre.this, mess);
       }
       else
          {
            if(txtnomMembre.getText().length()<3)
            {
            String incorrect="Nom Membre incorrect";
            JOptionPane.showMessageDialog(FenNouveauMembre.this, incorrect);
            } 
             else
             {
               String nom =txtnomMembre.getText();
               String prenom=txtprenomMemb.getText();
               String titre=cmbtitre.getSelectedItem().toString();
              
             
               Date datenaiss= dtpdatenais.getDate();
               sqldateNais= new java.sql.Date(datenaiss.getTime());
               String datenaisans =sqldateNais.toString();
               String statut= cmbstatut.getSelectedItem().toString();
               String cinif=txtCINIF.getText();
               String commune= cmbcommune.getSelectedItem().toString();
               String adres=txtadresse.getText();
               String tel=txttelephone.getText();
               String imel=txtmail.getText();
               String profes1= cmbprofe1.getSelectedItem().toString();
               String profes2= cmbprofe2.getText();
               String reference=txtrefere.getText();
               //String dtbap=txtdatbap.getText();
               Date dtbap=dtpdatebap.getDate();
               sqldateNais=new java.sql.Date(dtbap.getTime());
               String datebap =sqldateNais.toString();
               String fonction1= cmbfonction1.getSelectedItem().toString();
               String fonction2= cmbfonction2.getText();
               String relation= cmbrelation.getSelectedItem().toString();
            
               String reponse=ctrl.ValiderMembre(codeMemb,nom,prenom,titre,datenaisans,statut,cinif,commune,adres,tel,imel,profes1,profes2,reference,datebap,fonction1,fonction2,relation);
               JOptionPane.showMessageDialog(FenNouveauMembre.this, reponse);
               listerMembre();
               compterMembreactif();
               vider();
               //NettoyerComboBox();
               textEnabbledTrue();
                }
             }  
    }//GEN-LAST:event_btnValiderActionPerformed
    // Methode de recherche a partir du code
    private void btnOkMembreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOkMembreActionPerformed
      if(txtcodeMembre.getText().isEmpty())
      {
      JOptionPane.showMessageDialog(FenNouveauMembre.this,"Champs Vide");
      }
       else
          {
          rechercheMembre();
          }      
    }//GEN-LAST:event_btnOkMembreActionPerformed
    //Appel a la methode lister membre lorsque la fenetre est ouverte
    private void formInternalFrameOpened(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameOpened

     listerMembre();
     compterMembreactif();
       //int compte = tbMembre.getRowCount();
       //lblnbremembre.setText(""  +compte+"  Membre(s) actif(s)");
    }//GEN-LAST:event_formInternalFrameOpened
     //Methode pour compter les membre actif
    public void compterMembreactif(){
         int compte = tbMembre.getRowCount();
         lblnbremembre.setText(""  +compte+"  Membre(s) actif(s)");
    }
    //Appel a ces deux methodes lorsqu'on cliique sur le bouton annuler
    private void btnannuler1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnannuler1ActionPerformed
      existNM=false;
      this.dispose();
    }//GEN-LAST:event_btnannuler1ActionPerformed
    
    //Appel a la methode rechercherMembrePar
    private void txtrechercherparKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtrechercherparKeyReleased
     rehercherMembrePar();       
    }//GEN-LAST:event_txtrechercherparKeyReleased
  
    //Appel a la methode vider
    private void btnAnnulerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAnnulerActionPerformed
        vider();
    }//GEN-LAST:event_btnAnnulerActionPerformed
  
    //Implementation de la methode modifier
    private void btnmodifierActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnmodifierActionPerformed
     String relation1=cmbrelation.getSelectedItem().toString();
     String relation3="Inactif";
     String relation4="Livré";
     String relation5="Actif";
     
     if(txtnomMembre.getText().isEmpty()||txtprenomMemb.getText().isEmpty()||txtCINIF.getText().isEmpty()||txtcodeMembre.getText().isEmpty()||txtadresse.getText().isEmpty()||txttelephone.getText().isEmpty()|| txtmail.getText().isEmpty()||txtrefere .getText().isEmpty()||cmbprofe2.getText().isEmpty()||cmbfonction2.getText().isEmpty())
     {
       String mess;
       mess="Vous devez remplir toutes les cases";
       JOptionPane.showMessageDialog(FenNouveauMembre.this, mess);
      }
      else
      { 
         String code=txtcodeMembre.getText();
       String nom=txtnomMembre.getText();
       String prenom=txtprenomMemb.getText();
       String titre=cmbtitre.getSelectedItem().toString();       
       Date datenaiss=dtpdatenais.getDate();
       sqldateNais=new java.sql.Date(datenaiss.getTime());
       String datenaisans =sqldateNais.toString();
     
       String statut=cmbstatut.getSelectedItem().toString();
       String cinif=txtCINIF.getText();
       String commune=cmbcommune.getSelectedItem().toString();
       String adres=txtadresse.getText();
       String tel=txttelephone.getText();
       String imel=txtmail.getText();
       String profes1=cmbprofe1.getSelectedItem().toString();
       String profes2=cmbprofe2.getText();
       String reference=txtrefere.getText();
       //String dtbap=txtdatbap.getText();
       
       Date dtbap=dtpdatebap.getDate();
       sqldateNais=new java.sql.Date(dtbap.getTime());
       String datebap =sqldateNais.toString();
       
       String fonction1=cmbfonction1.getSelectedItem().toString();
       String fonction2=cmbfonction2.getText();
       String relation=cmbrelation.getSelectedItem().toString();
        String reponse=ctrl.ModifieMembre(code,nom, prenom, titre, datenaisans, statut, cinif, commune, adres, tel, imel, profes1, profes2, reference, datebap, fonction1, fonction2, relation);
        JOptionPane.showMessageDialog(FenNouveauMembre.this,reponse);
        listerMembre();
        compterMembreactif();
        vider();
       // NettoyerComboBox(); 
      }
    }//GEN-LAST:event_btnmodifierActionPerformed

    //Appel a la methode ImprimerTable
    private void btnImprimerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnImprimerActionPerformed
     ImprimerTable();        // TODO add your handling code here:
    }//GEN-LAST:event_btnImprimerActionPerformed
    //Methode qui met des evenements sur le control NomMembre
    private void txtnomMembreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtnomMembreKeyTyped
      char car=evt.getKeyChar();
       if ((Character.isDigit(car)|| (car==KeyEvent.VK_BACK_SPACE) || car==KeyEvent.VK_DELETE) )
       {
       evt.consume();
       }
        char keyChar = evt.getKeyChar();
         if (Character.isLowerCase(keyChar)) {
    evt.setKeyChar(Character.toUpperCase(keyChar));
}
    }//GEN-LAST:event_txtnomMembreKeyTyped
    
    //Methode qui formate le champ CINIF pour les CIN
    private void rbt_cinActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbt_cinActionPerformed
        MaskFormatter uppercase;
        try {
            uppercase = new MaskFormatter("##-##-##-####-##-#####");
            DefaultFormatterFactory factory = new DefaultFormatterFactory(uppercase);
            txtCINIF.setFormatterFactory(factory);
            txtCINIF.setEnabled(true);
            } 
            catch (ParseException ex)
           {
            Logger.getLogger(FenNouveauMembre.class.getName()).log(Level.SEVERE, null, ex);
           } 
    }//GEN-LAST:event_rbt_cinActionPerformed
    //Methode qui formate le champ CINIF pour les NIF
    private void rbt_nifActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbt_nifActionPerformed
      MaskFormatter uppercase;
        try {
            uppercase = new MaskFormatter("###-###-###-#");
            DefaultFormatterFactory factory = new DefaultFormatterFactory(uppercase);
            txtCINIF.setFormatterFactory(factory);
            txtCINIF.setEnabled(true);
        } catch (ParseException ex) {
            Logger.getLogger(FenNouveauMembre.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_rbt_nifActionPerformed

    //Methode qui met des evenements sur le control PrenomMembre
    private void txtprenomMembKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtprenomMembKeyTyped
      char car=evt.getKeyChar();
       if ((Character.isDigit(car)|| (car==KeyEvent.VK_BACK_SPACE) || car==KeyEvent.VK_DELETE) )
       {
       evt.consume();
       }       
    }//GEN-LAST:event_txtprenomMembKeyTyped

    private void tbMembreMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbMembreMouseClicked
        int index = tbMembre.getSelectedRow();
        TableModel mod = tbMembre.getModel();
        txtcodeMembre.setText(mod.getValueAt(index, 0).toString());  
        rechercheMembre();
          
    }//GEN-LAST:event_tbMembreMouseClicked

    //Appel a cette methode lors de la fermeture de la forme.
    private void formInternalFrameClosing(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameClosing
        existNM=false;
    }//GEN-LAST:event_formInternalFrameClosing

    private void txtmailKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtmailKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_txtmailKeyTyped
  
    //Methode avec action sur les items dans le combobox
    private void cmbprofe1ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbprofe1ItemStateChanged
    Object item=cmbprofe1.getSelectedItem();
    if ("Aucune".equals(item)) 
    {
      cmbprofe2.setText("Aucune");
      cmbprofe2.setEnabled(false);
     } 
      else if (("Autre".equals(item))||("Agronome".equals(item))||("Avocat".equals(item))||("Cameraman".equals(item))||("Comptable".equals(item))||("Charpente".equals(item))||("Chaufeur".equals(item))||("Gestionnaire".equals(item))||("Ebeniste".equals(item))||("Electricien".equals(item))||("Ing.Civil".equals(item))||("Infirmiere".equals(item))||("Ing.Informaticien".equals(item))||("Mecanicien".equals(item))||("Medecin".equals(item))||("Tailleur".equals(item))){
      cmbprofe2.setEnabled(true);
      cmbprofe2.setText("");
     }
    }//GEN-LAST:event_cmbprofe1ItemStateChanged

    private void cmbfonction1ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbfonction1ItemStateChanged
     Object item=cmbfonction1.getSelectedItem();
    if ("Aucune".equals(item)) 
    {
     cmbfonction2.setText("Aucune");
     cmbfonction2.setEnabled(false);
     } 
      else if (("Autre".equals(item))||("Administrateur".equals(item))||("Conducteur de chants".equals(item))||("Consiège".equals(item))||("Diacre".equals(item))||("Huissier".equals(item))||("Musicien".equals(item))||("Pasteur".equals(item))||("Predicateur".equals(item))){
      cmbfonction2.setEnabled(true);
      cmbfonction2.setText("");
     } 
 
    }//GEN-LAST:event_cmbfonction1ItemStateChanged
    
//Implementation de la methode RechercheMembre
    
    public void rechercheMembre()
    {   //String rela="Actif";
        String codemembre=txtcodeMembre.getText();
        txtcodeMembre.requestFocus();//positionne le focus sur le champ
         if(codemembre.length()==0)
        {
        JOptionPane.showMessageDialog(FenNouveauMembre.this, "Champ code vide!");  
         return;
        }
         boolean reponse=ctrl.RechercheMembre(txtcodeMembre.getText());
         if(!reponse)
         { 
           JOptionPane.showMessageDialog(FenNouveauMembre.this, " Ce code n'existe pas", "Error", JOptionPane.ERROR_MESSAGE );  
           }
             
           else{         
              //textEnabledFalse();
              textEnabbledTrue();
              txtnomMembre.setText(ctrl.PrendNomMem());
              txtprenomMemb.setText(ctrl.PrendPrenomMem());
              cmbtitre.setSelectedItem(ctrl.PrendTitreMem());     
              SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
               datenaissans =ctrl.PrendDateNaissMem();
               try{ 
                  dtpdatenais.setDate(formatter.parse(datenaissans));
                  }
                   catch (ParseException e){e.printStackTrace();}
              cmbstatut.setSelectedItem(ctrl.PrendStatutMem());
              txtCINIF.setText(ctrl.PrendCinif());
              cmbcommune.setSelectedItem(ctrl.PrendCommune());
              txtadresse.setText(ctrl.PrendAdresse());
              txttelephone.setText(ctrl.PrendTel());
              txtmail.setText(ctrl.PrendMail());
              cmbprofe1.setSelectedItem(ctrl.PrendProfession1());
              cmbprofe2.setText(ctrl.PrendProfession2());     
              txtrefere.setText(ctrl.PrendReference());
              datebapteme =ctrl.PrendDateBaptem();
               try{ 
                  dtpdatebap.setDate(formatter.parse(datebapteme));
                  }
                   catch (ParseException e){e.printStackTrace();}
              cmbfonction1.setSelectedItem(ctrl.PrendFonction1()); 
              cmbfonction2.setText(ctrl.PrendFonction2()); 
              cmbrelation.setSelectedItem(ctrl.PrendRelation());
              btnValider.setEnabled(false);
              btnmodifier.setEnabled(true);
            
        }
              
       }
               
   //Implementation de la methode listerMembre
    String rela="Actif";
    public void listerMembre()
    {
        int i=0;  
        String repons="";
        try 
           {
            String req="Select codeMembre as Code,titre as Titre,prenomMemb as Prenom,nomMemb as Nom,telephone as Telephone,fonction1 as Fonction,profession1 as Profession from membre where relation='"+rela+"'"; 
            
            pst=conn.prepareStatement(req);
            rs=pst.executeQuery();
            tbMembre.setModel(DbUtils.resultSetToTableModel(rs));
            }
             catch(SQLException se)
            {
                JOptionPane.showMessageDialog(null,"Erreur Requette"+ se.getMessage());   
            }
       }
    //Implementation de la methode rechercherMembrePar
     public void rehercherMembrePar()
     {
       if (cmbmemb.getSelectedItem().equals("Code Membre"))
       {
         String repons="";
        try 
           {
            String rek="Select codeMembre as Code,titre as Titre, nomMemb as Nom,prenomMemb as Prenoom,telephone as Telephone,fonction1 as Fonction,profession1 as Profession from membre where codeMembre like ?"; 
            
            pst=conn.prepareStatement(rek);
            pst.setString(1, txtrechercherpar.getText() + "%");
            rs = pst.executeQuery();
            tbMembre.setModel(DbUtils.resultSetToTableModel(rs));
             }
             catch(SQLException se)
             {
                JOptionPane.showMessageDialog(null,"Erreur Requette"+ se.getMessage());
                
              }
           }
            else if(cmbmemb.getSelectedItem().equals("Fonction"))
            {
            String repons="";
            try 
              {
            String rek="Select codeMembre as Code,titre as Titre,nomMemb as Nom,prenomMemb as Prenom,telephone as Telephone,fonction1 as Fonction,profession1 as Profession from membre where fonction1 like ?"; 
            
            pst=conn.prepareStatement(rek);
            pst.setString(1, txtrechercherpar.getText() + "%");
            rs = pst.executeQuery();
            tbMembre.setModel(DbUtils.resultSetToTableModel(rs));
             }
            catch(SQLException se)
            {
                JOptionPane.showMessageDialog(null,"Erreur Requette"+ se.getMessage());    
            }   
         }
           else if(cmbmemb.getSelectedItem().equals("Nom Membre"))
           {
              String repons="";
              try 
              {
              String rek="Select codeMembre as Code,titre as Titre,nomMemb as Nom,prenomMemb as Prenom,telephone as Telephone,fonction1 as Fonction,profession1 as Profession from Membre where nomMemb like ?"; 
              pst=conn.prepareStatement(rek);
              pst.setString(1, txtrechercherpar.getText() + "%");
              rs = pst.executeQuery();
              tbMembre.setModel(DbUtils.resultSetToTableModel(rs));
              }
              catch(SQLException se)
              {
                JOptionPane.showMessageDialog(null,"Erreur Requette"+ se.getMessage());   
              }    
             }
       }
 
     
     //Implementation de la methode vider les donnees dans les champs
      public void effacerDonnees(){
       txtnomMembre.setText("");
       txtprenomMemb.setText("");
       cmbtitre.setSelectedItem(0);
       //cmbsexe.setSelectedItem(0);
       dtpdatenais.setDate(null);
       cmbstatut.setSelectedItem(0);
       txtCINIF.setText("");
       cmbcommune.setSelectedItem(0);
       txtadresse.setText("");
       txttelephone.setText("");
       txtmail.setText("");
       cmbprofe1.setSelectedItem(0);
       cmbprofe2.setText("");
       txtrefere.setText("");
       dtpdatebap.setDate(null);
       cmbfonction1.setSelectedItem(0);
       cmbfonction2.setText("");
       cmbrelation.setSelectedItem(0);
             //dtpdatedeces.setDate(null);
       }
      //Implementation de la methode vider
       public void vider()
       {
        txtcodeMembre.setText("");
        txtnomMembre.setText("");
        txtprenomMemb.setText("");
        cmbtitre.setSelectedItem(null);
        dtpdatenais.setDate(null);
        cmbstatut.setSelectedItem(null);
        txtCINIF.setText("");
        cmbcommune.setSelectedItem(null);
        txtadresse.setText("");
        txttelephone.setText("");
        txtmail.setText("");
        dtpdatebap.setDate(null);
        cmbprofe1.setSelectedItem(null);
        cmbprofe2.setText("");
        txtrefere.setText("");
        cmbfonction1.setSelectedItem(null);
        cmbfonction2.setText("");
        cmbrelation.setSelectedItem(null);
         btnValider.setEnabled(true);
        btnmodifier.setEnabled(false);
        //dtpdatedeces.setDate(null);
        /*
        txtcodeMembre.setEnabled(false);
        txtnomMembre.setEnabled(false);
        txtprenomMemb.setEnabled(false);
        cmbtitre.setEnabled(false);
       // cmbsexe.setEnabled(false);
        dtpdatenais.setEnabled(false);
        cmbstatut.setEnabled(false);
        txtCINIF.setEnabled(false);
        cmbcommune.setEnabled(false);
        txtadresse.setEnabled(false);
        txttelephone.setEnabled(false);
        txtmail.setEnabled(false);
        cmbprofe1.setEnabled(false);
        cmbprofe2.setEnabled(false);
        txtrefere.setEnabled(false);
        dtpdatebap.setEnabled(false);
        cmbfonction1.setEnabled(false);
        cmbfonction2.setEnabled(false);
        cmbrelation.setEnabled(false);
             
       );
        */
        }
       //Implementation de la methode textEnabbledTrue
        private void textEnabbledTrue()
        {
         txtnomMembre.setEnabled(true);
         txtprenomMemb.setEnabled(true);
         cmbtitre.setEnabled(true);
         //cmbsexe.setEnabled(true);
         dtpdatenais.setEnabled(true);
         cmbstatut.setEnabled(true);
         //txtCINIF.setEnabled(true);
         cmbcommune.setEnabled(true);
         txtadresse.setEnabled(true);
         txttelephone.setEnabled(true);
         txtmail.setEnabled(true);
         cmbprofe1.setEnabled(true);
         cmbprofe2.setEnabled(true);
         txtrefere.setEnabled(true);
         dtpdatebap.setEnabled(true);
         cmbfonction1.setEnabled(true);
         cmbfonction2.setEnabled(true);
         cmbrelation.setEnabled(true);
         }
         public void textEnabledFalse()
         {
          txtnomMembre.setEnabled(false);
          txtprenomMemb.setEnabled(false);
          cmbtitre.setEnabled(false);
          //cmbsexe.setEnabled(false);
          dtpdatenais.setEnabled(false);
          cmbstatut.setEnabled(false);
          txtCINIF.setEnabled(false);
          cmbcommune.setEnabled(false);
          txtadresse.setEnabled(false);
          txttelephone.setEnabled(false);
          txtmail.setEnabled(false);
          cmbprofe1.setEnabled(false);
          cmbprofe2.setEnabled(false);
          txtrefere.setEnabled(false);
          dtpdatebap.setEnabled(false);
          cmbfonction1.setEnabled(false);
          cmbfonction2.setEnabled(false);
          cmbrelation.setEnabled(false);
         }
      //Implementation de la methode ImprimerTable
      public void ImprimerTable()
      {
       MessageFormat header = new MessageFormat("Liste des Membres");
       MessageFormat footer= new MessageFormat("Page {0,number,interger}");
       MessageFormat details=new MessageFormat("Yblad", Locale.FRENCH);
       try
       {
        tbMembre.print(JTable.PrintMode.FIT_WIDTH, header,footer);
        // tblisteachat.print(JTable.PrintMode.FIT_WIDTH, header, footer);
        } 
       catch (java.awt.print.PrinterException se)
       {
       System.err.format("Impossible d'imprimer:", se.getMessage());
       }
}
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAnnuler;
    private javax.swing.JButton btnImprimer;
    private javax.swing.JButton btnOkMembre;
    private javax.swing.JButton btnValider;
    private javax.swing.JButton btnannuler1;
    private javax.swing.ButtonGroup btncin_nif;
    private javax.swing.JButton btnmodifier;
    private javax.swing.JComboBox<String> cmbcommune;
    private javax.swing.JComboBox<String> cmbfonction1;
    private javax.swing.JTextField cmbfonction2;
    private javax.swing.JComboBox cmbmemb;
    public javax.swing.JComboBox cmbprofe1;
    private javax.swing.JTextField cmbprofe2;
    private javax.swing.JComboBox<String> cmbrelation;
    private javax.swing.JComboBox<String> cmbstatut;
    private javax.swing.JComboBox<String> cmbtitre;
    private com.toedter.calendar.JDateChooser dtpdatebap;
    private com.toedter.calendar.JDateChooser dtpdatenais;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblnbremembre;
    private javax.swing.JRadioButton rbt_cin;
    private javax.swing.JRadioButton rbt_nif;
    private javax.swing.JTable tbMembre;
    private javax.swing.JFormattedTextField txtCINIF;
    private javax.swing.JTextField txtadresse;
    private javax.swing.JTextField txtcodeMembre;
    private javax.swing.JTextField txtmail;
    private javax.swing.JTextField txtnomMembre;
    private javax.swing.JTextField txtprenomMemb;
    private javax.swing.JTextField txtrechercherpar;
    private javax.swing.JTextField txtrefere;
    private javax.swing.JFormattedTextField txttelephone;
    // End of variables declaration//GEN-END:variables
}
