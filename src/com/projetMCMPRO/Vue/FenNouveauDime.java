
package com.projetMCMPRO.Vue;

import javax.swing.JOptionPane;

import com.projetMCMPRO.Controleur.ControleurDime;
import com.projetMCMPRO.Dal.mysqlconnect;
import java.awt.event.KeyEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import javax.swing.JTable;
import javax.swing.table.TableModel;

import net.proteanit.sql.DbUtils;


public class FenNouveauDime extends javax.swing.JInternalFrame {
    public static Connection conn;
    public static Statement st;
    
    private static final long serialVersionUID = 1L;
    public ResultSet rs;
    public PreparedStatement pst = null;
    public java.sql.Date sqldateNais, sqldateDeces,sqldatePai;
    String datepaiement;
    public TableModel model;
    public int indexligne;
    
    private void NettoyerComboBox(){
    cmbtitre.setSelectedItem(null);
    cmbmonaie.setSelectedItem(null);
    dtpDatepaiement.setDate(null);
  
 
    }
    public  ControleurDime ctrlD =new ControleurDime();
   public String codeMemb, co;
   public  String Codime;
   public static  boolean existND;
   
    public FenNouveauDime() {
        conn=mysqlconnect.ConnectDB();
        existND=true;
        initComponents();
        NettoyerComboBox();
       btnmodifier.setEnabled(false);
    }
   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        txtcodeMembre = new javax.swing.JTextField();
        btnRechercherMembre = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        cmbtitre = new javax.swing.JComboBox<String>();
        jLabel1 = new javax.swing.JLabel();
        txtnomMembre = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtprenomMemb = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        txttelephone = new javax.swing.JFormattedTextField();
        jLabel11 = new javax.swing.JLabel();
        codepristext = new javax.swing.JTextField();
        btnValider = new javax.swing.JButton();
        btnmodifier = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        cmbmemb = new javax.swing.JComboBox();
        jLabel7 = new javax.swing.JLabel();
        txtrechercherpar = new javax.swing.JTextField();
        btnquitter = new javax.swing.JButton();
        btnAnnuler = new javax.swing.JButton();
        btnImprimer = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        cmbmonaie = new javax.swing.JComboBox<String>();
        jLabel10 = new javax.swing.JLabel();
        txtmontant = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        dtpDatepaiement = new com.toedter.calendar.JDateChooser();
        jPanel5 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tbdime = new javax.swing.JTable();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbMembre = new javax.swing.JTable();
        jLabel5 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        lblEur = new javax.swing.JLabel();
        lblcad = new javax.swing.JLabel();
        lblusd = new javax.swing.JLabel();
        lblhtg = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();

        setClosable(true);
        setTitle("Nouveau Dime");
        addInternalFrameListener(new javax.swing.event.InternalFrameListener() {
            public void internalFrameActivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosed(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosing(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameClosing(evt);
            }
            public void internalFrameDeactivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeiconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameIconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameOpened(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameOpened(evt);
            }
        });

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel2.setText("Code Membre :");

        btnRechercherMembre.setText("Rechercher");
        btnRechercherMembre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRechercherMembreActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtcodeMembre, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnRechercherMembre, javax.swing.GroupLayout.DEFAULT_SIZE, 112, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(txtcodeMembre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(jLabel2)
                .addComponent(btnRechercherMembre))
        );

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Informations sur le memebre"));

        jLabel4.setText("Titre:");

        cmbtitre.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Frere", "Soeur", " " }));
        cmbtitre.setEnabled(false);

        jLabel1.setText("Nom :");

        txtnomMembre.setEnabled(false);
        txtnomMembre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtnomMembreKeyTyped(evt);
            }
        });

        jLabel3.setText("Prenom:");

        txtprenomMemb.setEnabled(false);
        txtprenomMemb.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtprenomMembKeyTyped(evt);
            }
        });

        jLabel8.setText("Telephone :");

        try {
            txttelephone.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("(509)####-####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txttelephone.setEnabled(false);

        jLabel11.setText("Code:");

        codepristext.setEnabled(false);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(jLabel3)
                            .addGap(27, 27, 27)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel8)
                        .addGap(10, 10, 10)))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txttelephone)
                    .addComponent(txtprenomMemb)
                    .addComponent(cmbtitre, 0, 278, Short.MAX_VALUE)
                    .addComponent(codepristext)
                    .addComponent(txtnomMembre)))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(codepristext, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbtitre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtnomMembre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtprenomMemb, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(txttelephone, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        btnValider.setText("Valider");
        btnValider.setEnabled(false);
        btnValider.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnValiderActionPerformed(evt);
            }
        });

        btnmodifier.setText("Modifier");
        btnmodifier.setEnabled(false);
        btnmodifier.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnmodifierActionPerformed(evt);
            }
        });

        jLabel6.setText("Lister Par:");

        cmbmemb.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Code Membre", "Titre", "Nom Membre" }));

        jLabel7.setText("Taper....");

        txtrechercherpar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtrechercherparKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap(99, Short.MAX_VALUE)
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cmbmemb, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(43, 43, 43)
                .addComponent(jLabel7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtrechercherpar, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(jLabel6)
                .addComponent(cmbmemb, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(jLabel7)
                .addComponent(txtrechercherpar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        btnquitter.setText("Quitter");
        btnquitter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnquitterActionPerformed(evt);
            }
        });

        btnAnnuler.setText("Annuler");
        btnAnnuler.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAnnulerActionPerformed(evt);
            }
        });

        btnImprimer.setText("Imprimer");
        btnImprimer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnImprimerActionPerformed(evt);
            }
        });

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder("Informations sur la dime"));

        jLabel9.setText("Monnaie:");

        cmbmonaie.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "HTG", "USD", "CAD", "EUR", "DOP", "BRL" }));
        cmbmonaie.setEnabled(false);

        jLabel10.setText("Montant:");

        txtmontant.setEnabled(false);
        txtmontant.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtmontantKeyTyped(evt);
            }
        });

        jLabel13.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        jLabel13.setText("Date Paiement:");

        dtpDatepaiement.setDateFormatString("dd-MM-yyyy");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(jLabel9)
                        .addComponent(jLabel10))
                    .addComponent(jLabel13))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtmontant, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(cmbmonaie, 0, 297, Short.MAX_VALUE)
                    .addComponent(dtpDatepaiement, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addGap(0, 19, Short.MAX_VALUE)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbmonaie, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9))
                .addGap(18, 18, 18)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtmontant, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10))
                .addGap(18, 18, 18)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel13)
                    .addComponent(dtpDatepaiement, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jPanel5.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        tbdime.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        tbdime.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tbdime.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbdimeMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tbdime);

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2)
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
        );

        tbMembre.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tbMembre.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbMembreMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                tbMembreMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                tbMembreMouseExited(evt);
            }
        });
        jScrollPane1.setViewportView(tbMembre);

        jLabel5.setText("USD");

        jLabel12.setText("CAD");

        jLabel14.setText("EUR");

        jLabel15.setText("HTG");

        lblEur.setText("jLabel16");

        lblcad.setText("jLabel17");

        lblusd.setText("jLabel18");

        lblhtg.setText("jLabel19");

        jLabel20.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel20.setText("Total Dime par monnaie");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(btnValider, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnmodifier)
                        .addGap(28, 28, 28)
                        .addComponent(btnAnnuler, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnquitter, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(13, 13, 13)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addComponent(btnImprimer)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 14, Short.MAX_VALUE)
                        .addComponent(jLabel20)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(lblhtg)
                        .addGap(2, 2, 2)
                        .addComponent(jLabel15)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(lblusd)
                        .addGap(2, 2, 2)
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(lblcad)
                        .addGap(1, 1, 1)
                        .addComponent(jLabel12)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblEur)
                        .addGap(4, 4, 4)
                        .addComponent(jLabel14)
                        .addGap(13, 13, 13))
                    .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel5, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(0, 1, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(13, 13, 13)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel5)
                        .addComponent(jLabel14)
                        .addComponent(jLabel12)
                        .addComponent(jLabel15)
                        .addComponent(lblEur)
                        .addComponent(lblcad)
                        .addComponent(lblusd)
                        .addComponent(lblhtg)
                        .addComponent(jLabel20))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnImprimer)
                        .addComponent(btnquitter)
                        .addComponent(btnValider, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnAnnuler, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnmodifier)))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    
    //Methode d'enregistrement d'un membre
   
    private void btnValiderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnValiderActionPerformed
    String tyepemonai=(String)cmbmonaie.getSelectedItem();
    cmbmonaie.requestFocus();//positionne le focus sur le champ
    if(tyepemonai==null){
    JOptionPane.showMessageDialog(null, "Champ type de monnaie vide! S'il vous plait selectionnez un");  
    return;
    }
      
     
    if(txtmontant.getText().isEmpty())// 
     {
        String mess;
        mess="Vous devez remplir toutes les cases";
        JOptionPane.showMessageDialog(FenNouveauDime.this, mess);
      }
        else
        {      
             
             String codeMembre=codepristext.getText();
             int cod=(int)(Math.random()*1230);
             String codeDime=codeMembre+"-"+cod;
             
             String monai=cmbmonaie.getSelectedItem().toString();
             double montan= new Double(txtmontant.getText());
            Date dtpaie=dtpDatepaiement.getDate();
            sqldateNais=new java.sql.Date(dtpaie.getTime());
            String datepaie =sqldateNais.toString();
             String repons=ctrlD.ValiderDime(codeDime,codeMembre,monai, montan, datepaie);
            JOptionPane.showMessageDialog(FenNouveauDime.this, repons);
            listerDime();
            calculerdime();
            vider();
            vider2();
         }
         
    }//GEN-LAST:event_btnValiderActionPerformed
    
    // Methode de recherche a partir du code
    private void btnRechercherMembreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRechercherMembreActionPerformed
       if(txtcodeMembre.getText().isEmpty())
        {
         JOptionPane.showMessageDialog(FenNouveauDime.this,"Champs Vide");
        }
         else
        {
         rechercheMembre();
        }      
    }//GEN-LAST:event_btnRechercherMembreActionPerformed

    private void formInternalFrameOpened(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameOpened
     listerMembre();
     listerDime();
     calculerdime();
    }//GEN-LAST:event_formInternalFrameOpened

    private void btnquitterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnquitterActionPerformed
      existND=false;
        this.dispose();
    }//GEN-LAST:event_btnquitterActionPerformed

    private void txtrechercherparKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtrechercherparKeyReleased
     rehercherMembrePar();       
    }//GEN-LAST:event_txtrechercherparKeyReleased

    private void btnAnnulerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAnnulerActionPerformed
        vider();
    }//GEN-LAST:event_btnAnnulerActionPerformed

    private void btnmodifierActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnmodifierActionPerformed
         
             String monai=cmbmonaie.getSelectedItem().toString();
             double montan= new Double(txtmontant.getText());
             Date dtpaie=dtpDatepaiement.getDate();
            sqldateNais=new java.sql.Date(dtpaie.getTime());
            String datepaie =sqldateNais.toString();
             
        if(txtmontant.getText().isEmpty())
        {
            String mess;
            mess="Vous devez remplir toutes les cases";

            JOptionPane.showMessageDialog(FenNouveauDime.this, mess);
        }
        else
        {
            String reponse=ctrlD.ModifierDime(Codime,codeMemb, monai, montan,datepaie);
            JOptionPane.showMessageDialog(FenNouveauDime.this, reponse);
            listerDime();
            calculerdime();
            vider();
        } 
    
    }//GEN-LAST:event_btnmodifierActionPerformed

    private void btnImprimerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnImprimerActionPerformed
     ImprimerTable();        // TODO add your handling code here:
    }//GEN-LAST:event_btnImprimerActionPerformed

    private void txtnomMembreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtnomMembreKeyTyped
      char car=evt.getKeyChar();
       if ((Character.isDigit(car)|| (car==KeyEvent.VK_BACK_SPACE) || car==KeyEvent.VK_DELETE) )
       {
       evt.consume();
       }
    }//GEN-LAST:event_txtnomMembreKeyTyped

    private void txtprenomMembKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtprenomMembKeyTyped
      char car=evt.getKeyChar();
       if ((Character.isDigit(car)|| (car==KeyEvent.VK_BACK_SPACE) || car==KeyEvent.VK_DELETE) )
       {
       evt.consume();
       }       
    }//GEN-LAST:event_txtprenomMembKeyTyped

    //Methode permettant de selectioner un membre dans liste et faire aussi la recherche
    private void tbMembreMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbMembreMouseClicked
   vider();
        int index = tbMembre.getSelectedRow();
    TableModel mod = tbMembre.getModel();
    txtcodeMembre.setText(mod.getValueAt(index, 0).toString());  
    rechercheMembre();
    listerDimeparmembre();
    }//GEN-LAST:event_tbMembreMouseClicked

    private void formInternalFrameClosing(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameClosing
        existND=false;
    }//GEN-LAST:event_formInternalFrameClosing
    
    //Methode sur le Jtable dime
    private void tbdimeMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbdimeMouseClicked
       vider();
       SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        indexligne = tbdime.getSelectedRow();
        model = tbdime.getModel();
        Codime = model.getValueAt(indexligne,0).toString();
        txtcodeMembre.setText(model.getValueAt(indexligne, 1).toString());
        //************************************
          // indexligne = tbdime.getSelectedRow();
          //   model = tbdime.getModel();
          //   Codime = model.getValueAt(indexligne,0).toString();
           //  codeMemb = model.getValueAt(indexligne,1).toString();
        //************************************
        rechercheMembre();
        try
        {
            String req="Select * from dime where codeDime='"+Codime+"' ";
            st=conn.createStatement();
            rs=st.executeQuery(req);
            if(rs.next())
            {
                cmbmonaie.setSelectedItem(rs.getString("type_monnaie"));
                cmbmonaie.setEnabled(true);
                txtmontant.setText(rs.getString("montant"));
                txtmontant.setEnabled(true);
                 datepaiement =rs.getString("date_paiement");
               try{ 
                  dtpDatepaiement.setDate(formatter.parse(datepaiement));
                  }
                   catch (ParseException e){e.printStackTrace();}
                codepristext.setText(rs.getString("noMembre"));
                 btnmodifier.setEnabled(true);
                 btnValider.setEnabled(false);
                
            }
        }
        catch(SQLException se)
        {
            JOptionPane.showMessageDialog(null,"Erreur Requette"+ se.getMessage());

        }

    }//GEN-LAST:event_tbdimeMouseClicked

    private void tbMembreMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbMembreMouseExited
       
    }//GEN-LAST:event_tbMembreMouseExited

    private void tbMembreMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbMembreMouseEntered
       listerDime(); 
    }//GEN-LAST:event_tbMembreMouseEntered

    private void txtmontantKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtmontantKeyTyped
      if(!Character.isDigit(evt.getKeyChar())&& evt.getKeyChar()!='.' )
        {
        evt.consume();
            }
        if(evt.getKeyChar()=='.'&&txtmontant.getText().contains("."))
            {
        evt.consume();
         }
    }//GEN-LAST:event_txtmontantKeyTyped
 
    
//Methode permettant de faire la recherche a partir du code membre    
public void rechercheMembre()
{
 String idmembr= txtcodeMembre.getText();
  String rela="actif";
        try 
        {
            String req="Select * from membre where codeMembre='"+idmembr+"' and relation='"+rela+"' "; 
            st=conn.createStatement();
            rs=st.executeQuery(req);
            if(rs.next())
            {
             
              //textEnabbledTrue();
              codeMemb=rs.getString("codeMembre");
              codepristext.setText(rs.getString("codeMembre"));
              txtnomMembre.setText(rs.getString("nomMemb"));
              txtprenomMemb.setText(rs.getString("prenomMemb"));
              cmbtitre.setSelectedItem(rs.getString("titre"));
              txttelephone.setText(rs.getString("telephone"));
              btnValider.setEnabled(true);
              btnRechercherMembre.setEnabled(false);
              cmbmonaie.setEnabled(true);
              txtmontant.setEnabled(true);
              dtpDatepaiement.setEnabled(true);;
             }
             else
            {
            JOptionPane.showMessageDialog(FenNouveauDime.this,"Ce membre n'existe pas");
            }
           }
            catch(SQLException se)
            {
            JOptionPane.showMessageDialog(null,"Erreur Requette"+ se.getMessage());    
            }      
      }
    //Methode permet de lsiter les membre
   public void listerMembre()
  {
        int i=0;
          String rela="actif"; 
        String repons="";
        try 
        {
            
            String req="Select codeMembre as Code,titre as Titre,nomMemb as Nom, prenomMemb as Prenom,telephone as Telephone from membre where relation ='"+rela+"'"; 
           
            pst=conn.prepareStatement(req);
            rs=pst.executeQuery();
            tbMembre.setModel(DbUtils.resultSetToTableModel(rs));
        }
        catch(SQLException se)
            {
                JOptionPane.showMessageDialog(null,"Erreur Requette"+ se.getMessage());
                
            }
}
   
//Methode de recherche par Code, Titre et nom du membre
 public void rehercherMembrePar()
   {
       String rela="actif";
     if (cmbmemb.getSelectedItem().equals("Code Membre"))
     {
     String repons="";
     
        try 
        {
            String rek="Select codeMembre as Code_Membre,titre as Titre, nomMemb as Nom, prenomMemb as Prenom,telephone as Telephone from membre where relation='"+rela+"' and codeMembre like ?"; 
            
            pst=conn.prepareStatement(rek);
            pst.setString(1, txtrechercherpar.getText() + "%");
            rs = pst.executeQuery();
            tbMembre.setModel(DbUtils.resultSetToTableModel(rs));
             }
        
            catch(SQLException se)
            {
                JOptionPane.showMessageDialog(null,"Erreur Requette"+ se.getMessage());
                
            }
     }
     else if(cmbmemb.getSelectedItem().equals("Titre"))
     {
       String repons="";
        try 
        {
            String rek="Select codeMembre as Code_Membre,titre as Titre,nomMemb as Nom, prenomMemb as Prenom,telephone as Telephone from membre where relation='"+rela+"' and titre like ?"; 
           
            pst=conn.prepareStatement(rek);
            pst.setString(1, txtrechercherpar.getText() + "%");
            rs = pst.executeQuery();
            tbMembre.setModel(DbUtils.resultSetToTableModel(rs));
             }
        
            catch(SQLException se)
            {
                JOptionPane.showMessageDialog(null,"Erreur Requette"+ se.getMessage());
                
            }   
     }
     else if(cmbmemb.getSelectedItem().equals("Nom Membre"))
     {
     String repons="";
        try 
        {
            String rek="Select codeMembre as Code_Membre,titre as Titre,nomMemb as Nom, prenomMemb as Prenom,telephone as Telephone from Membre where relation='"+rela+"' and nomMemb like ?"; 
          
            pst=conn.prepareStatement(rek);
            pst.setString(1, txtrechercherpar.getText() + "%");
            rs = pst.executeQuery();
            tbMembre.setModel(DbUtils.resultSetToTableModel(rs));
             }
        
            catch(SQLException se)
            {
                JOptionPane.showMessageDialog(null,"Erreur Requette"+ se.getMessage());
                
            }    
     }
   }
 
    public void listerDime()
  {
      String noMembre=codepristext.getText();
        int i=0;
           
        String repons="";
        try 
        {
            String req="Select codeDime as Dime,noMembre as Membre, type_monnaie as Type,montant as Montant, date_paiement from dime "; 
            
            pst=conn.prepareStatement(req);
            rs=pst.executeQuery();
            tbdime.setModel(DbUtils.resultSetToTableModel(rs));
        }
        catch(SQLException se)
            {
                JOptionPane.showMessageDialog(null,"Erreur Requette"+ se.getMessage());
                
            }
}
 public void listerDimeparmembre()
  {
      String noMembre=codepristext.getText();
        int i=0;
           
        String repons="";
        try 
        {
            String req="Select codeDime as Dime,noMembre as Membre, type_monnaie as Type,montant as Montant, date_paiement from dime where noMembre='"+noMembre+"'"; 
            
            pst=conn.prepareStatement(req);
            rs=pst.executeQuery();
            tbdime.setModel(DbUtils.resultSetToTableModel(rs));
        }
        catch(SQLException se)
            {
                JOptionPane.showMessageDialog(null,"Erreur Requette"+ se.getMessage());
                
            }
} 
 public void calculerdime()
 {
     double usd=0.00;
     double htg=0.00;
     double cad=0.00;
     double eur=0.00;
      try 
        {
            String req="Select * from dime";
            st=conn.createStatement();
            rs = st.executeQuery(req);
            while(rs.next())
            {
               switch (rs.getString("type_monnaie")) {
         case "HTG":
            htg=htg+ Double.parseDouble(rs.getString("montant"));
             break;
         case "USD":
            usd=usd+ Double.parseDouble(rs.getString("montant"));
             break;
         case "CAD":
           cad=cad+ Double.parseDouble(rs.getString("montant"));
             break;
         case "EUR":
           eur=eur+ Double.parseDouble(rs.getString("montant"));
             break;
     }
            }
            lblhtg.setText(""+htg);
            lblusd.setText(""+usd);
            lblEur.setText(""+eur);
            lblcad.setText(""+cad);
        }
         catch(SQLException se)
            {
                JOptionPane.showMessageDialog(null,"Erreur Requette"+ se.getMessage());   
            } 
   
 
 }
 
  public void vider2()
 {           
   cmbmonaie.setSelectedItem(0); 
   txtmontant.setText("");
   cmbmonaie.setEnabled(true);
   txtmontant.setEnabled(true);
   dtpDatepaiement.setDate(null);
 } 
 public void vider()
 {
             txtcodeMembre.setText("");
             codepristext.setText("");
             txtnomMembre.setText("");
             txtprenomMemb.setText("");
             txtmontant.setText("");
             cmbtitre.setSelectedItem(null);
             dtpDatepaiement.setDate(null);
             cmbmonaie.setSelectedItem(null);
             txttelephone.setText("");
             //cmbmonaie.setSelectedItem(0);
             
             txtcodeMembre.setEnabled(false);
             txtnomMembre.setEnabled(false);
             txtprenomMemb.setEnabled(false);
             cmbtitre.setEnabled(false);
             txttelephone.setEnabled(false);
             txtmontant.setEnabled(false);
             dtpDatepaiement.setEnabled(false);
             cmbmonaie.setEnabled(false);
             btnValider.setEnabled(true);
             btnmodifier.setEnabled(false);
         
 }
 public void textEnabbledTrue()
 {          txtnomMembre.setEnabled(true);
             txtprenomMemb.setEnabled(true);
             cmbtitre.setEnabled(true);
             txttelephone.setEnabled(true);         
 }
 public void textEnabledFalse()
 {
             txtnomMembre.setEnabled(false);
             txtprenomMemb.setEnabled(false);
             cmbtitre.setEnabled(false);
             txttelephone.setEnabled(false);
        
 }
 
 public void ImprimerTable()
{
   MessageFormat header = new MessageFormat("Liste des Dime");
   MessageFormat footer= new MessageFormat("Page {0,number,interger}");
   MessageFormat details=new MessageFormat("Yblad", Locale.FRENCH);
    try {
        tbMembre.print(JTable.PrintMode.FIT_WIDTH, header,footer);

    } catch (java.awt.print.PrinterException se) {
        System.err.format("Impossible d'imprimer:", se.getMessage());
    }


}
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAnnuler;
    private javax.swing.JButton btnImprimer;
    private javax.swing.JButton btnRechercherMembre;
    private javax.swing.JButton btnValider;
    private javax.swing.JButton btnmodifier;
    private javax.swing.JButton btnquitter;
    private javax.swing.JComboBox cmbmemb;
    private javax.swing.JComboBox<String> cmbmonaie;
    private javax.swing.JComboBox<String> cmbtitre;
    private javax.swing.JTextField codepristext;
    private com.toedter.calendar.JDateChooser dtpDatepaiement;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblEur;
    private javax.swing.JLabel lblcad;
    private javax.swing.JLabel lblhtg;
    private javax.swing.JLabel lblusd;
    private javax.swing.JTable tbMembre;
    private javax.swing.JTable tbdime;
    private javax.swing.JTextField txtcodeMembre;
    private javax.swing.JTextField txtmontant;
    private javax.swing.JTextField txtnomMembre;
    private javax.swing.JTextField txtprenomMemb;
    private javax.swing.JTextField txtrechercherpar;
    private javax.swing.JFormattedTextField txttelephone;
    // End of variables declaration//GEN-END:variables
}
