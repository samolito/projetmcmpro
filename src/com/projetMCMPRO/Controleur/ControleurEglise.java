/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projetMCMPRO.Controleur;

import com.projetMCMPRO.Dal.EgliseDal;
import com.projetMCMPRO.Domaine.Eglise;

/**
 *
 * @author INGSAMUEL
 */
public class ControleurEglise {
   public Eglise egl;
  public ControleurEglise()
  {
  egl= new Eglise();
  }
   public String PrendreCodeEglise()
  {
      return this.egl.getCodEglise();
  }
   public  void ModifierCodeEglise(String codEg)
   {
   this.egl.setCodEglise(codEg);
   } 
  public String PrendreNomEgl()
  {
      return this.egl.getNomEglise();
  }
   public  void ModifierNomEgl(String nomEg)
   {
   this.egl.setNomEglise(nomEg);
   } 
   public String PrendreNompast()
  {
      return this.egl.getNomPast();
  }
   public  void ModifierNompast(String nompast)
   {
   this.egl.setNomPast(nompast);
   } 
   public String PrendreAdresse()
  {
   return this.egl.getAdresse();
  }
   public  void ModifierAdresse(String adres)
   {
   this.egl.setAdresse(adres);
   }
   public String PrendreTel1()
  {
   return this.egl.getTelephone1();
  }
   public  void ModifierTel1(String tel1)
   {
   this.egl.setTelephone1(tel1);
   }
   
   public String PrendreTel2()
  {
   return this.egl.getTelephone2();
  }
   public  void ModifierTel2(String tel2)
   {
   this.egl.setTelephone1(tel2);
   }
   public String ValiderEglise(String codegl,String nomEgl, String nompast,String adres,String tel1,String tel2)
   {
   this.egl=new Eglise(codegl,nomEgl, nompast, adres, tel1, tel2);
   return EgliseDal.EnregistrerEglise(this.egl);
   }
   
   public String ModifierEglise(String codexpo,String nomEgl,String nompast, String adres,String tel1,String tel2)
   {
   this.egl= new Eglise(codexpo, nomEgl,nompast, adres, tel1, tel2);
   return EgliseDal.ModifierEglise(this.egl);
   }  
}
