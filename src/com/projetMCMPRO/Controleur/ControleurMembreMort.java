package com.projetMCMPRO.Controleur;
import com.projetMCMPRO.Dal.MembreMortDal;
import com.projetMCMPRO.Domaine.MembreMort;


public class ControleurMembreMort {
  public MembreMort memb;
  public MembreMortDal dal;
  
   public ControleurMembreMort()
   {
    memb= new MembreMort();
   dal=new MembreMortDal();
   }
  
  public String PrendreID()
  {
   return this.memb.getIdMembre();
  }
  public  void ModifierID(String idmemb)
  {
  this.memb.setIdMembre(idmemb);
  }
   
   public String PrendreDateDeces()
   {
      return this.memb.getDateDeces();
   }
   public  void ModifierDateDeces(String dtdeces)
   {
   this.memb.setDateDeces(dtdeces);
   }
   
   public String PrendreCause()
   {
      return this.memb.getCause();
   }
   public  void ModifierCause(String cause)
   {
   this.memb.setCause(cause);
   }
   
  public String PrendreDateFune()
   {
      return this.memb.getDateFune();
   }
   public  void ModifierDateFune(String dtfune)
   {
   this.memb.setDateFun(dtfune);
   }
   
   
   
   //Les proprites de la classe MembreDal
   public String PrendID()
   {
       
   return this.dal.getIdMembre();
   }
  
   public String PrendDateDeces()
   {
      return this.dal.getDateDeces();
   }
   public String PrendCause()
   {
      return this.dal.getCause();
   }
    public String PrendDateFune()
   {
      return this.dal.getDatefune();
   }
   public String ValiderMembreMort(String co,String datdeces,String cause,String datfune)
   {
   this.memb=new MembreMort(co,datdeces,cause,datfune);
   return MembreMortDal.EnregistrerMembreMort(this.memb);
   }  
   
  public  boolean RechercheMembreMort(String code)
  {
  this.memb=new MembreMort(code);
  return MembreMortDal.RechercherMembreMort(this.memb);
  }
   
  public String ModifierMembreMort(String co, String datdeces,String cause,String datfune)
 {
   this.memb=new MembreMort(co,datdeces,cause,datfune);
   return MembreMortDal.ModifierMembreMort(this.memb);
  }
   
}
