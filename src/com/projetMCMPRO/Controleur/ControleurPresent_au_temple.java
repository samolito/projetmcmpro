/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projetMCMPRO.Controleur;

import com.projetMCMPRO.Dal.Presentation_au_templeDAL;
import com.projetMCMPRO.Domaine.Presentation_au_temple;

/**
 *
 * @author INGSAMUEL
 */
public class ControleurPresent_au_temple {
    public Presentation_au_temple pat;
  
   public ControleurPresent_au_temple()
   {
    pat= new Presentation_au_temple();
   }
  public String PrendreMembre()
  {
   return this.pat.getMembre();
  }
  public  void ModifierMembre(String memb)
  {
  this.pat.setMembre(memb);
  }
  public String PrendreID()
  {
   return this.pat.getNoMembre();
  }
  public  void ModifierID(String idmemb)
  {
  this.pat.setNoMembre(idmemb);
  }
   
   public String PrendreNomEnfant()
   {  
    return this.pat.getNomEnfant();	
   }
   public  void ModifierNomEnfant(String no)
   {
   this.pat.setNomenfant(no);
   }
   public String PrendrePrenomEnfant()
   {
      return this.pat.getPrenomEnfant();
   }
   public  void ModifierPrenomEnfant(String pre)
   {
   this.pat.setPrenomenfant(pre);
   }
   public String PrendreDatenaissance()
   {
     return this.pat.getDate_naissance();
   }
   public  void ModifierDatenaissance(String datn)
   {
   this.pat.setDate_naissance(datn);
   }
    public String Prendrelieunaissance()
   {
     return this.pat.getlieu_naissance();
   }
   public  void ModifierLieunaissance(String lieun)
   {
   this.pat.setlieu_naissance(lieun);
   }
    public String PrendreNompere()
   {  
    return this.pat.getNompere();
   }
   public  void ModifierNompere(String nomp)
   {
   this.pat.setNommere(nomp);
   }
   public String PrendreNommere()
   {
      return this.pat.getNommere();
   }
   public  void ModifierNommere(String nomm)
   {
   this.pat.setNommere(nomm);
   }
   public String PrendreDatepresent()
   {
      return this.pat.getDate_presentation();
   }
   public  void ModifierDatepresent(String datpr)
   {
   this.pat.setDate_presentation(datpr);
   }
    public String ValiderPresentation(String memb, String nomemb,String nom, String pren,String datnais,String lieun,String nomp, String nomm,String datpres)
   {
   this.pat=new Presentation_au_temple(memb, nomemb, nomemb, pren, datnais, lieun, nomp, nomm, datpres);
   return Presentation_au_templeDAL.EnregistrerPresent_au_temple(this.pat);
   }  
    public String ModifierPresentation(String memb, String nomemb,String nom, String pren,String datnais,String lieun,String nomp, String nomm,String datpres)
   {
   this.pat=new Presentation_au_temple(memb, nomemb, nomemb, pren, datnais, lieun, nomp, nomm, datpres);
   return Presentation_au_templeDAL.ModifierPresentation_au_temple(this.pat);
   }
}
