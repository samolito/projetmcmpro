/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projetMCMPRO.Controleur;

import com.projetMCMPRO.Dal.OffrandeDal;
import com.projetMCMPRO.Domaine.Offrande;

/**
 *
 * @author INGSAMUEL
 */
public class ControleurOffrande {
 public Offrande off;

  
   public ControleurOffrande()
   {
    off= new Offrande();
   }
  
   public String PrendreNumOff()
  {
  return this.off.getNumero();
  }
  public  void ModifierNumOff(String num)
  {
  this.off.setNumero(num);
  }
   public String PrendreNomService()
   {  
    return this.off.getNom_service();	
   }
   public  void ModifierNomService(String mnomserv)
   {
   this.off.setNom_service(mnomserv);
   }
   public String PrendreMonnaie()
   {  
    return this.off.getMonnaie();	
   }
   public  void ModifierMonnaie(String monaie)
   {
   this.off.setMonnaie(monaie);
   }
   public double PrendreMontant()
   {
      return this.off.getMontant();
   }
   public  void ModifierMontant(double montan)
   {
   this.off.setMontant(montan);
   }
   
   public String PrendreDateOff()
   {
      return this.off.getDate_off();
   }
   public  void ModifierDateOff(String dateoff)
   {
   this.off.setDate_off(dateoff);
   }
    
   public String ValiderOffrande(String num,String nomServ,String monaie,double montant,String datedim)
   {
   this.off=new Offrande(num, nomServ,monaie, montant, datedim);
   return OffrandeDal.EnregistrerOffrande(this.off);
   }
   public String ModifierOffrande(String num,String nomServ,String monaie,double montant,String datedim)
   {
   this.off=new Offrande(num, nomServ,monaie, montant, datedim);
   return OffrandeDal.ModifierOffrandeDAL(this.off);
   }
}
