package com.projetMCMPRO.Controleur;
import com.projetMCMPRO.Dal.DimeDal;
import com.projetMCMPRO.Domaine.Dime;

public class ControleurDime {
  public Dime dim;

  
   public ControleurDime()
   {
    dim= new Dime();
   }
  
   public String PrendreIDDime()
  {
  return this.dim.getCodeDime();
  }
  public  void ModifierIDDime(String iddim)
  {
  this.dim.setCodeDime(iddim);
  }
 public String PrendreID()
  {
  return this.dim.getCodeMembre();
  }
  public  void ModifierID(String idmemb)
  {
  this.dim.setCodeMembre(idmemb);
  }
   
   public String PrendreMonnaie()
   {  
    return this.dim.getMonnaie();	
   }
   public  void ModifierMonnaie(String monaie)
   {
   this.dim.setMonnaie(monaie);
   }
   
   public double PrendreMontant()
   {
      return this.dim.getMontant();
   }
   public  void ModifierMontant(double montan)
   {
   this.dim.setMontant(montan);
   }
   
   public String PrendreDatedime()
   {
      return this.dim.getDateDime();
   }
   public  void ModifierDatedime(String datedim)
   {
   this.dim.setDateDime(datedim);
   }
  
   
  
   public String ValiderDime(String codeDime,String codeMembre,String monnaie,double montant,String datedim)
   {
   this.dim=new Dime(codeDime,codeMembre,monnaie,montant,datedim);
   return DimeDal.EnregistrerDime(this.dim);
   }  
   
  
   
 public String ModifierDime(String Codime,String codeMembre,String monai,double montan,String datedime)
  {
   this.dim=new Dime(Codime,codeMembre,monai, montan, datedime);
   return DimeDal.ModifierDime(this.dim);
  }
    

   /*
   public  boolean RechercheMembre(String code)
  {
   this.memb=new Membre(code);
   return MembreDal.RechercherMembre(this.memb);
  }
  public String DeplacerMembre(String code)
  {
   this.memb=new Membre(code);
   return MembreDal.SupprimerMembre(this.memb);
  }
   */

  
   
}
