/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projetMCMPRO.Controleur;

import com.projetMCMPRO.Dal.EgliseFilleDal;
import com.projetMCMPRO.Domaine.Eglisefille;

/**
 *
 * @author INGSAMUEL
 */
public class ControleurEgliseFille {
    public Eglisefille egl;
  public ControleurEgliseFille()
  {
  egl= new Eglisefille();
  }
   public String PrendreCodeEglise()
  {
      return this.egl.getCodEglise();
  }
   public  void ModifierCodeEglise(String codEg)
   {
   this.egl.setCodEglise(codEg);
   } 
  public String PrendreNomEgl()
  {
      return this.egl.getNomEglise();
  }
   public  void ModifierNomEgl(String nomEg)
   {
   this.egl.setNomEglise(nomEg);
   } 
   public String PrendreNompast()
  {
      return this.egl.getNomPasteur();
  }
   public  void ModifierNompast(String nompast)
   {
   this.egl.setNomPasteur(nompast);
   } 
    public String PrendreNomDiacre()
  {
      return this.egl.getNomDiacre();
  }
   public  void ModifierNomDiacre(String nomdiac)
   {
   this.egl.setNomDiacre(nomdiac);
   } 
    public String PrendreDepartement()
  {
   return this.egl.getDepartement();
  }
   public  void ModifierDepartement(String dep)
   {
   this.egl.setDepartement(dep);
   }
   public String Prendrecommune()
  {
   return this.egl.getCommune();
  }
   public  void ModifierTel2(String com)
   {
   this.egl.setCommune(com);
   }
   
   public String PrendreAdresse()
  {
   return this.egl.getAdresse();
  }
   public  void ModifierAdresse(String adres)
   {
   this.egl.setAdresse(adres);
   }
   public String PrendreEtat()
  {
   return this.egl.getEtat();
  }
   public  void ModifierEtat(String eta)
   {
   this.egl.setEtat(eta);
   }
   public String ValiderEglisefille(String codegl,String nomEgl, String nompast,String diacre,String depart,String comm,String adres,String eta)
   {
   this.egl=new Eglisefille(codegl, nomEgl, nompast, diacre, depart, comm, adres,eta);
   return EgliseFilleDal.EnregistrerEglisefille(this.egl);         
   }
   
   public String ModifierEglise(String codegl,String nomEgl, String nompast,String diacre,String depart,String comm,String adres,String eta)
   {
   this.egl= new Eglisefille(codegl, nomEgl, nompast, diacre, depart, comm, adres,eta);
   return EgliseFilleDal.ModifierEglisefille(this.egl);
   }     
   public String DeplacerEglise(String codegl,String nomEgl, String nompast,String diacre,String depart,String comm,String adres,String eta)
   {
   this.egl= new Eglisefille(codegl, nomEgl, nompast, diacre, depart, comm, adres,eta);
   return EgliseFilleDal.SupprimerMembre(this.egl);
   }
}
