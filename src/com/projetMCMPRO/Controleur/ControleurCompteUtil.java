package com.projetMCMPRO.Controleur;
import com.projetMCMPRO.Dal.CompteUtilDal;
import com.projetMCMPRO.Domaine.CompteUtil;



public class ControleurCompteUtil {
   public CompteUtil cmputil;
   
  public ControleurCompteUtil()
  {
  cmputil= new CompteUtil();
  }
  public String PrendreCodeUtil()
  {
      return this.cmputil.getCodeUtil();
  } 
  public void ModifierCodeUtil(String codutil)
  {
  this.cmputil.setCodeUtil(codutil);
  }
    public String PrendrenomUtil()
  {
      return this.cmputil.getNom();
  } 
  public void ModifiernomUtil(String nomutil)
  {
  this.cmputil.setPrenom(nomutil);
  } 
   public String PrendrePrenomUtil()
  {
      return this.cmputil.getPrenom();
  } 
  public void ModifierPrenomUtil(String prenomutil)
  {
  this.cmputil.setPrenom(prenomutil);
  } 
   public String PrendreCin_nif()
  {
      return this.cmputil.getCin_Nif();
  } 
  public void ModifierCi_nif(String cinif)
  {
  this.cmputil.setCin_Nif(cinif);
  } 
  public String PrendreAdresse()
  {
      return this.cmputil.getAdresse();
  } 
  public void ModifierAdresse(String adres)
  {
  this.cmputil.setAdresse(adres);
  } 
   public String PrendreTelephone()
  {
      return this.cmputil.getTelephone();
  } 
  public void ModifierTelephone(String Tel)
  {
  this.cmputil.setTelephone(Tel);
  }
  
  public String PrendreFonction()
  {
      return this.cmputil.getFonction();
  } 
  public void ModifierFonction(String fonc)
  {
  this.cmputil.setFonction(fonc);
  }
  public String PrendreUsername()
  {
      return this.cmputil.getNom_utilisateur();
  } 
  public void ModifierUsername(String util)
  {
  this.cmputil.setNom_utilisateur(util);
  }
  public String PrendreMotdepasse()
  {
      return this.cmputil.getMot_de_passe();
  } 
  public void ModifierMotdepasse(String mdp)
  {
  this.cmputil.setMot_de_passe(mdp);
  }
  
  public String PrendreEtatCompte()
  {
      return this.cmputil.getEtat_Compte();
  } 
  public void ModifierEtatCompte(String etat)
  {
  this.cmputil.setEtat_Compte(etat);
  }
  
  public  String ValiderCompteUtil(String codUtil, String nom, String pre, String cinif, String adres, String tel, String fonction, String username,String modpas, String etat)
  {
  this.cmputil= new CompteUtil(codUtil, nom, pre, cinif, adres,tel, fonction, username, modpas, etat);
  return CompteUtilDal.EnregistrerCompteUtil(this.cmputil);
  }
  
  public String ModifierCompteUtil(String codUtil, String nom, String pre, String cinif, String adres, String tel, String fonction, String username, String modpas, String etat)
  {
  this.cmputil= new CompteUtil(codUtil, nom, pre, cinif, adres,tel, fonction, username, modpas, etat);
  return CompteUtilDal.ModifierCompteUtil(this.cmputil);
  }
  
 public String SupprimerCompteUtil(String codUtil, String nom, String pre, String cinif, String adres, String tel, String fonction, String username, String modpas, String etat)
  {
  this.cmputil= new CompteUtil(codUtil, nom, pre, cinif, adres,tel, fonction, username, modpas, etat);
  return CompteUtilDal.SupprimerCompteUtil(this.cmputil);
  } 
}
