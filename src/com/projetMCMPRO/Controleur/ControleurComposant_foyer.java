/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projetMCMPRO.Controleur;

import com.projetMCMPRO.Dal.Composant_foyerDal;
import com.projetMCMPRO.Domaine.composant_foyer;

/**
 *
 * @author INGSAMUEL
 */
public class ControleurComposant_foyer {
    public composant_foyer comp_foy;
  
   public ControleurComposant_foyer()
   {
    comp_foy= new composant_foyer();
   }
  
  public String PrendreID()
  {
   return this.comp_foy.getNoMembre();
  }
  public  void ModifierID(String idmemb)
  {
  this.comp_foy.setNoMembre(idmemb);
  }
   
   public String PrendreNom()
   {  
    return this.comp_foy.getNom();	
   }
   public  void ModifierNom(String nomm)
   {
   this.comp_foy.setNom(nomm);
   }
   
   public String PrendrePrenom()
   {
      return this.comp_foy.getPrenom();
   }
   public  void ModifierPrenomMem(String pre)
   {
   this.comp_foy.setPrenom(pre);
   }
   public String PrendreLienparente()
   {
     return this.comp_foy.getLien_parente();
   }
   public  void ModifierLienparente(String lienp)
   {
   this.comp_foy.setLien_parente(lienp);
   }
   public String PrendreMembreON()
  {
   return this.comp_foy.getMembreON();
  }
  public  void ModifiermembreON(String memb)
  {
  this.comp_foy.setMembreON(memb);
  }
    public String Validercomposant_Foyer(String nomemb, String nom,String pren,String lienp,String membon)
   {
   this.comp_foy=new composant_foyer(nomemb, nom, pren, lienp,membon);
   return Composant_foyerDal.EnregistrerComposantFoyer(this.comp_foy);
   }  
   
  public String Modifiercomposant_Foyer(String nomemb, String nom,String pren,String lienp,String membon)
   {
   this.comp_foy=new composant_foyer(nomemb, nom, pren, lienp,membon);
   return Composant_foyerDal.ModifierComposantFoyer(this.comp_foy);
   }  
}
