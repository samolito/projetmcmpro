package com.projetMCMPRO.Controleur;
import com.projetMCMPRO.Dal.MembreDal;
import com.projetMCMPRO.Domaine.Membre;

public class ControleurMembre {
  public Membre memb;
  public MembreDal dal;
  
   public ControleurMembre()
   {
    memb= new Membre();
    dal=new MembreDal();
   }
  
  public String PrendreID()
  {
   return this.memb.getCodeMembre();
  }
  public  void ModifierID(String idmemb)
  {
  this.memb.setCodeMembre(idmemb);
  }
   
   public String PrendreNomMem()
   {  
    return this.memb.getNomMembre();	
   }
   public  void ModifierNomMem(String nommembr)
   {
   this.memb.setNomMembre(nommembr);
   }
   
   public String PrendrePrenomMem()
   {
      return this.memb.getPrenomMembre();
   }
   public  void ModifierPrenomMem(String prenommem)
   {
   this.memb.setPrenomMembre(prenommem);
   }
   
   public String PrendreTitreMem()
   {
      return this.memb.getTitreMembre();
   }
   public  void ModifierTitreMem(String titremembr)
   {
   this.memb.setTitreMembre(titremembr);
   }
   public String PrendreDateNaissMem()
   {
     return this.memb.getDateNaissMembre();
   }
   public  void ModifierDateNaissMem(String datenaissmembr)
   {
   this.memb.setDatNaissMembre(datenaissmembr);
   }
   
   public String PrendreStatutMem()
   {
     return this.memb.getStatMatMembre();
   }
   public  void ModifierStatutMem(String statutmembr)
   {
   this.memb.setStatMatMembre(statutmembr);
   }
   
  public String PrendreCinif()
  {
      return this.memb.getCINIF();
  }
   public  void ModifierCinif(String cinf)
   {
   this.memb.setCINIF(cinf);
   }
   
   public String PrendreCommune()
   {
      return this.memb.getCommune();
   }
   public  void ModifierCommune(String comu)
   {
   this.memb.setCommune(comu);
   }
   
  public String PrendreAdresse()
  {
      return this.memb.getAdresse();
  }
   public  void ModifierAdresse(String adres)
   {
   this.memb.setAdresse(adres);
   }
   
   public String PrendreTel()
   {
      return this.memb.getTelephone();
   }
   public  void ModifierTel(String tel)
   {
   this.memb.setTelephone(tel);
   }
   
  public String PrendreMail()
  {
   return this.memb.getMail();
  }
  public  void ModifierMail(String mail)
  {
   this.memb.setEmail(mail);
  }
   
  public String PrendreProfession1()
  {
      return this.memb.getProfession1();
  }
   public  void ModifierProfession1(String profes1)
   {
   this.memb.setProfession1(profes1);
   }
   
   public String PrendreProfession2()
   {
      return this.memb.getProfession2();
   }
   public  void ModifierProfession2(String profes2)
   {
   this.memb.setProfession2(profes2);
   }
   
   public String PrendreReference()
   {
      return this.memb.getReference();
   }
   public  void ModifierReference(String referen)
   {
   this.memb.setReference(referen);
   }
   
   public String PrendreDateBaptem()
   {
      return this.memb.getDateBapteme();
   }
   public  void ModifierDateBapteme(String datbap)
   {
   this.memb.setDateBapteme(datbap);
   }
   
   public String PrendreFonction1()
   {
      return this.memb.getFonction1();
   }
   public  void ModifierFonction1(String fonc1)
   {
   this.memb.setFonction1(fonc1);
   }
   
   public String PrendreFonction2()
   {
      return this.memb.getFonction2();
   }
   public  void ModifierFonction2(String fonc2)
   {
   this.memb.setFonction2(fonc2);
   }
     
   public String PrendreRelation()
   {
      return this.memb.getRelation();
   }
   public  void ModifierRelation(String rela)
   {
   this.memb.setRelation(rela);
   }
   
   
   
   //Les proprites de la classe MembreDal
   public String PrendID()
   {
       
   return this.dal.getIdMembre();
   }
   public String PrendNomMem()
   {  
    return this.dal.getNomMembre();	
   }
   public String PrendPrenomMem()
   {
      return this.dal.getPrenomMembre();
   }
   public String PrendTitreMem()
   {
    return this.dal.getTitreMembre();
   }
   public String PrendDateNaissMem()
   {
   return this.dal.getDateNaissMembre();
   }
   public String PrendStatutMem()
   {
     return this.dal.getStatMatMembre();
   }
   
  public String PrendCinif()
  {
      return this.dal.getCINIF();
  }
  
  public String PrendCommune()
   {
      return this.dal.getCommune();
   }

  public String PrendAdresse()
  {
      return this.dal.getAdresse();
  }
   
   public String PrendTel()
   {
      return this.dal.getTelephone();
   }
   
  public String PrendMail()
  {
      return this.dal.getMail();
  } 
  public String PrendProfession1()
  {
      return this.dal.getProfession1();
  }
   public String PrendProfession2()
   {
      return this.dal.getProfession2();
   }
   public String PrendReference()
   {
      return this.dal.getReference();
   }
  
   public String PrendDateBaptem()
   {
      return this.dal.getDateBapteme();
   }
   
   public String PrendFonction1()
   {
      return this.dal.getFonction1();
   }
   
   public String PrendFonction2()
   {
      return this.dal.getFonction2();
   }  
   public String PrendRelation()
   {
      return this.dal.getRelation();
   }
   
   public String ValiderMembre(String co, String nom,String pren,String titre,String datnaiss,String statmat,String cinif,String commune,String adres,String tel,String imel,String profes1,String profes2,String refere,String datbapte,String fonctio1,String fonctio2,String rela)
   {
   this.memb=new Membre(co,nom,pren,titre,datnaiss,statmat,cinif,commune,adres,tel,imel,profes1,profes2,refere,datbapte,fonctio1,fonctio2,rela);
   return MembreDal.EnregistrerMembre(this.memb);
   }  
   
   public  boolean RechercheMembre(String code)
  {
   this.memb=new Membre(code);
   return MembreDal.RechercherMembre(this.memb);
  }
   
  public String ModifieMembre(String co,String nom, String pren, String titre, String datnaiss, String statmat, String cinif, String commune, String adres, String tel, String imel, String profes1, String profes2, String refere, String datbapte, String fonctio1, String fonctio2, String rela)
  {
   this.memb=new Membre(co, nom, pren, titre, datnaiss, statmat, cinif, commune, adres, tel, imel, profes1, profes2, refere, datbapte, fonctio1, fonctio2, rela);
   return MembreDal.ModifierMembre(this.memb);
  }
  
  public String DeplacerMembre(String code)
  {
   this.memb=new Membre(code);
   return MembreDal.SupprimerMembre(this.memb);
  }
   
}
