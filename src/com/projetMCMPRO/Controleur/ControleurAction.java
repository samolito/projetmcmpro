/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projetMCMPRO.Controleur;
import com.projetMCMPRO.Dal.ActionDal;
import com.projetMCMPRO.Domaine.action;

public class ControleurAction {
  public action act;
  public ControleurAction()
  {
  act= new action();
  } 
  public String PrendreCodutil()
  {
      return this.act.getCodeUtil();
  }
   public  void ModifierCodeUtil(String codut)
   {
   this.act.setCodeUtil(codut);
   } 
    public String PrendreFonction()
  {
   return this.act.getFonction();
  }
   public  void ModifierFonction(String fonc)
   {
   this.act.setFonction(fonc);
   }
   public String PrendreAction()
  {
   return this.act.getActions();
  }
   public  void ModifierAction(String action)
   {
   this.act.setActions(action);
   }
   public String PrendreDate()
  {
   return this.act.getDateaction();
  }
   public  void ModifierDate(String dat)
   {
   this.act.setDateaction(dat);
   }
   
   public String PrendreHeure()
  {
   return this.act.getHeureaction();
  }
   public  void ModifierHeure(String hr)
   {
   this.act.setHeureaction(hr);
   }
   public String ValiderAction(String codutil,String fonc,String action, String dateac,String hrAc)
   {
   this.act=new action(codutil,fonc, action, dateac, hrAc);
   return ActionDal.EnregistrerAction(this.act);
   }
    
}
