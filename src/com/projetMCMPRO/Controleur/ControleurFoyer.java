/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projetMCMPRO.Controleur;

import com.projetMCMPRO.Dal.FoyerDal;
import com.projetMCMPRO.Domaine.foyer;

/**
 *
 * @author INGSAMUEL
 */
public class ControleurFoyer {
 public foyer foy;
  
   public ControleurFoyer()
   {
    foy= new foyer();
   }
  
  public String PrendreID()
  {
   return this.foy.getCoMembre();
  }
  public  void ModifierID(String idmemb)
  {
  this.foy.setCoMembre(idmemb);
  }
   
   public String PrendreNomMem()
   {  
    return this.foy.getNomMembre();	
   }
   public  void ModifierNomMem(String nommembr)
   {
   this.foy.setNomMembre(nommembr);
   }
   
   public String PrendrePrenomMem()
   {
      return this.foy.getPrenomMembre();
   }
   public  void ModifierPrenomMem(String prenommem)
   {
   this.foy.setPrenomMembre(prenommem);
   }
   public String PrendreStatutMem()
   {
     return this.foy.getStatmatMembre();
   }
   public  void ModifierStatutMem(String statutmembr)
   {
   this.foy.setStatmatMembre(statutmembr);
   }
   public String PrendreTelephone()
   {
      return this.foy.getTelephone();
   }
   public  void ModifierTelephone(String tel)
   {
   this.foy.setTelephone(tel);
   }
   public String Prendrenomconjoint()
   {
     return this.foy.getNomconjoint();
   }
   public  void Modifiernomconjoint(String nc)
   {
   this.foy.setNomconjoint(nc);
   }
   public int Prendrenbreenf()
   {
     return this.foy.getNbreenfeant();
   }
   public  void ModifiernbreEnf(int nbref)
   {
   this.foy.setNbreenfeant(nbref);
   }
    public int Prendrenbrepers()
   {
     return this.foy.getNbrepers();
   }
   public  void ModifiernbrePers(int pers)
   {
   this.foy.setNbrepers(pers);
   } 
    public String ValiderFoyer(String co, String nom,String pren,String statmat,String tel,String nomc,int nbrenf, int nbrep)
   {
   this.foy=new foyer(co, nom, pren, statmat, tel, nomc, nbrenf, nbrep);
   return FoyerDal.EnregistrerFoyer(this.foy);
   }  
   
   public String ModifierFoyer(String co, String nom,String pren,String statmat,String tel,String nomc,int nbrenf, int nbrep)
   {
   this.foy=new foyer(co, nom, pren, statmat, tel, nomc, nbrenf, nbrep);
   return FoyerDal.ModifierFoyer(this.foy);
   }  
}
