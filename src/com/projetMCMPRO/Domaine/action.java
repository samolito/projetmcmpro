/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projetMCMPRO.Domaine;

/**
 *
 * @author SAMY_PJ
 */
public class action {
  private String codeUtil;
  private String fonction;
  private String actions;
  private String dateaction;
  private String heureaction;
  public action()
   {
   }  
  public action(String codut,String fonc,String act,String dat,String heur)
   {
       this.codeUtil=codut;
       this.fonction=fonc;
       this.actions=act;
       this.dateaction=dat;
       this.heureaction=heur;
   }

    public String getCodeUtil() {
        return codeUtil;
    }

    public void setCodeUtil(String codeUtil) {
        this.codeUtil = codeUtil;
    }

    public String getFonction() {
        return fonction;
    }

    public void setFonction(String fonction) {
        this.fonction = fonction;
    }

    public String getActions() {
        return actions;
    }

    public void setActions(String actions) {
        this.actions = actions;
    }

    public String getDateaction() {
        return dateaction;
    }

    public void setDateaction(String dateaction) {
        this.dateaction = dateaction;
    }

    public String getHeureaction() {
        return heureaction;
    }

    public void setHeureaction(String heureaction) {
        this.heureaction = heureaction;
    }
  
}
