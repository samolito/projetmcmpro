/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projetMCMPRO.Domaine;

/**
 *
 * @author SAMY_PJ
 */
public class CompteUtil {
    private String CodeUtil;
    private String Nom;
    private String Prenom;
    private String Cin_Nif; 
    private String Adresse;
    private String Telephone;
    private String Fonction;
    private String Nom_utilisateur;
    private String Mot_de_passe;
    private String Etat_Compte;
   
     public CompteUtil() {
    }

    public CompteUtil(String code, String nom, String prenom, String cin_nif,String Adresse, String Telephone, String fonction, String nom_utilisateur, String mot_de_passe, String etat_Compte) {
        this.CodeUtil = code;
        this.Nom = nom;
        this.Prenom = prenom;
        this.Cin_Nif=cin_nif;
        this.Adresse = Adresse;
        this.Telephone = Telephone;
        this.Fonction = fonction;
        this.Nom_utilisateur = nom_utilisateur;
        this.Mot_de_passe = mot_de_passe;
        this.Etat_Compte = etat_Compte;
    }

    public String getCodeUtil() {
        return CodeUtil;
    }

    public void setCodeUtil(String CodeUtil) {
        this.CodeUtil = CodeUtil;
    }

    public String getNom() {
        return Nom;
    }

    public void setNom(String Nom) {
        this.Nom = Nom;
    }

    public String getPrenom() {
        return Prenom;
    }

    public void setPrenom(String Prenom) {
        this.Prenom = Prenom;
    }

    public String getCin_Nif() {
        return Cin_Nif;
    }

    public void setCin_Nif(String Cin_Nif) {
        this.Cin_Nif = Cin_Nif;
    }

    public String getAdresse() {
        return Adresse;
    }

    public void setAdresse(String Adresse) {
        this.Adresse = Adresse;
    }

    public String getTelephone() {
        return Telephone;
    }

    public void setTelephone(String Telephone) {
        this.Telephone = Telephone;
    }

    public String getFonction() {
        return Fonction;
    }

    public void setFonction(String Fonction) {
        this.Fonction = Fonction;
    }

    public String getNom_utilisateur() {
        return Nom_utilisateur;
    }

    public void setNom_utilisateur(String Nom_utilisateur) {
        this.Nom_utilisateur = Nom_utilisateur;
    }

    public String getMot_de_passe() {
        return Mot_de_passe;
    }

    public void setMot_de_passe(String Mot_de_passe) {
        this.Mot_de_passe = Mot_de_passe;
    }

    public String getEtat_Compte() {
        return Etat_Compte;
    }

    public void setEtat_Compte(String Etat_Compte) {
        this.Etat_Compte = Etat_Compte;
    }

   
    
}
