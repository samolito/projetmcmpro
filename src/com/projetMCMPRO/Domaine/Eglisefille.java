/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projetMCMPRO.Domaine;

/**
 *
 * @author INGSAMUEL
 */
public class Eglisefille {
   private String codEglise;
   private String NomEglise;
   private String nomPasteur;
  private String nomDiacre;
  private String departement;
  private String commune; 
  private String adresse;
  private String etat;

    public Eglisefille() {
    }

    public Eglisefille(String codEglise, String NomEglise, String nomPasteur, String nomDiacre, String departement, String commune, String adresse,String et) {
        this.codEglise = codEglise;
        this.NomEglise = NomEglise;
        this.nomPasteur = nomPasteur;
        this.nomDiacre = nomDiacre;
        this.departement = departement;
        this.commune = commune;
        this.adresse = adresse;
        this.etat=et;
    }

    public String getCodEglise() {
        return codEglise;
    }

    public void setCodEglise(String codEglise) {
        this.codEglise = codEglise;
    }

    public String getNomEglise() {
        return NomEglise;
    }

    public void setNomEglise(String NomEglise) {
        this.NomEglise = NomEglise;
    }

    public String getNomPasteur() {
        return nomPasteur;
    }

    public void setNomPasteur(String nomPasteur) {
        this.nomPasteur = nomPasteur;
    }

    public String getNomDiacre() {
        return nomDiacre;
    }

    public void setNomDiacre(String nomDiacre) {
        this.nomDiacre = nomDiacre;
    }

    public String getDepartement() {
        return departement;
    }

    public void setDepartement(String departement) {
        this.departement = departement;
    }

    public String getCommune() {
        return commune;
    }

    public void setCommune(String commune) {
        this.commune = commune;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }
  
    public String getEtat() {
        return etat;
    }

    public void setEtat(String eta) {
        this.etat = eta;
    }
}
