/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projetMCMPRO.Domaine;

/**
 *
 * @author INGSAMUEL
 */
public class Eglise {
   private String codEglise;
   private String nomPasteur;
  private String NomEglise;
  private String adresse;
  private String telephone1;
  private String telephone2;
  public Eglise()
   {
   }  
  public Eglise(String codegli,String nomegl,String nompast,String adres,String tel1,String tel2)
   {
       this.codEglise=codegli;
       this.nomPasteur=nompast;
       this.NomEglise=nomegl;
       this.adresse=adres;
       this.telephone1=tel1;
       this.telephone2=tel2;
   }

    public String getCodEglise() {
        return codEglise;
    }

    public void setCodEglise(String codEeg) {
        this.codEglise = codEeg;
    }
    
    public String getNomEglise() {
        return NomEglise;
    }

    public void setNomEglise(String NomEg) {
        this.NomEglise = NomEg;
    }
    
    public String getNomPast() {
        return nomPasteur;
    }

    public void setNomPast(String Nompast) {
        this.nomPasteur = Nompast;
    }
    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getTelephone1() {
        return telephone1;
    }

    public void setTelephone1(String telephone1) {
        this.telephone1 = telephone1;
    }

    public String getTelephone2() {
        return telephone2;
    }

    public void setTelephone2(String telephone2) {
        this.telephone2 = telephone2;
    }   
}
