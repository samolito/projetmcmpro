/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projetMCMPRO.Domaine;

/**
 *
 * @author INGSAMUEL
 */
public class Offrande {
 private String numero;
   private String nom_service;
   private String monnaie;
   private double montant;
   private String date_off;   

    public Offrande() {
    }

    public Offrande(String numero, String nom_service,String monnaie, double montant, String date_off) {
        this.numero = numero;
        this.nom_service = nom_service;
        this.monnaie = monnaie;
        this.montant = montant;
        this.date_off = date_off;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getNom_service() {
        return nom_service;
    }

    public void setNom_service(String nom_service) {
        this.nom_service = nom_service;
    }
    public String getMonnaie() {
        return monnaie;
    }

    public void setMonnaie(String monaie) {
        this.monnaie = monaie;
    }
    public double getMontant() {
        return montant;
    }

    public void setMontant(double montant) {
        this.montant = montant;
    }

    public String getDate_off() {
        return date_off;
    }

    public void setDate_off(String date_off) {
        this.date_off = date_off;
    }
   
   
}
