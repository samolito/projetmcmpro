/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projetMCMPRO.Domaine;

/**
 *
 * @author INGSAMUEL
 */
public class foyer {
   private String CoMembre;
   private String nomMembre;
   private String prenomMembre;
   private String statmatMembre;
   private String telephone;
   private String nomconjoint;
   private int nbreenfeant;
   private int nbrepers;

    public foyer() {
    }

    public foyer(String CoMembre, String nomMembre, String prenomMembre, String statmatMembre, String telephone, String nomconjoint, int nbreenfeant, int nbrepers) {
        this.CoMembre = CoMembre;
        this.nomMembre = nomMembre;
        this.prenomMembre = prenomMembre;
        this.statmatMembre = statmatMembre;
        this.telephone = telephone;
        this.nomconjoint = nomconjoint;
        this.nbreenfeant = nbreenfeant;
        this.nbrepers = nbrepers;
    }

    public String getCoMembre() {
        return CoMembre;
    }

    public void setCoMembre(String CoMembre) {
        this.CoMembre = CoMembre;
    }

    public String getNomMembre() {
        return nomMembre;
    }

    public void setNomMembre(String nomMembre) {
        this.nomMembre = nomMembre;
    }

    public String getPrenomMembre() {
        return prenomMembre;
    }

    public void setPrenomMembre(String prenomMembre) {
        this.prenomMembre = prenomMembre;
    }

    public String getStatmatMembre() {
        return statmatMembre;
    }

    public void setStatmatMembre(String statmatMembre) {
        this.statmatMembre = statmatMembre;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getNomconjoint() {
        return nomconjoint;
    }

    public void setNomconjoint(String nomconjoint) {
        this.nomconjoint = nomconjoint;
    }

    public int getNbreenfeant() {
        return nbreenfeant;
    }

    public void setNbreenfeant(int nbreenfeant) {
        this.nbreenfeant = nbreenfeant;
    }

    public int getNbrepers() {
        return nbrepers;
    }

    public void setNbrepers(int nbrepers) {
        this.nbrepers = nbrepers;
    }
   
}
