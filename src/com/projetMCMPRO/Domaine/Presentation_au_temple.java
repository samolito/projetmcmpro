/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projetMCMPRO.Domaine;

/**
 *
 * @author INGSAMUEL
 */
public class Presentation_au_temple {
   private String Membre;
   private String noMembre;
   private String nomenfant;
   private String prenomenfant;
    private String nommere;
   private String nompere;
    private String lieunais;
   private String date_naissance;
   private String date_presentation;

    public Presentation_au_temple() {
    }

    public Presentation_au_temple(String Membre, String noMembre, String nomenf, String prenomenf, String date_naissance,String lieunais,String nompere, String nommere, String date_presentation) {
        this.Membre = Membre;
        this.noMembre = noMembre;
        this.nomenfant= nomenf;
        this.prenomenfant = prenomenf;
        this.date_naissance = date_naissance;
        this.lieunais=lieunais;
         this.nompere= nompere;
        this.nommere= nommere;
        this.date_presentation = date_presentation;
    }

    public String getMembre() {
        return Membre;
    }

    public void setMembre(String Membre) {
        this.Membre = Membre;
    }

    public String getNoMembre() {
        return noMembre;
    }

    public void setNoMembre(String noMembre) {
        this.noMembre = noMembre;
    }

    public String getNomEnfant() {
        return nomenfant;
    }

    public void setNomenfant(String no) {
        this.nomenfant = no;
    }

    public String getPrenomEnfant() {
        return prenomenfant;
    }

    public void setPrenomenfant(String pren) {
        this.prenomenfant= pren;
    }
     public String getDate_naissance() {
        return date_naissance;
    }

    public void setDate_naissance(String lieu_naissance) {
        this.lieunais= lieu_naissance;
    }
     public String getlieu_naissance() {
        return lieunais;
    }

    public void setlieu_naissance(String lieu_naissance) {
        this.lieunais = lieu_naissance;
    }
 public String getNompere() {
        return nompere;
    }

    public void setNompere(String nop) {
        this.nompere = nop;
    }

    public String getNommere() {
        return nommere;
    }

    public void setNommere(String nomm) {
        this.nommere = nomm;
    }
   
    public String getDate_presentation() {
        return date_presentation;
    }

    public void setDate_presentation(String date_presentation) {
        this.date_presentation = date_presentation;
    }
   
}
