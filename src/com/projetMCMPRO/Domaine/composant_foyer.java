/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projetMCMPRO.Domaine;

/**
 *
 * @author INGSAMUEL
 */
public class composant_foyer {
  private String noMembre; 
  private String nom;
  private String prenom;
  private String lien_parente;
  private String MembreON;

    public composant_foyer() {
    }

    public composant_foyer(String noMembre, String nom, String prenom, String lien_parente,String membon) {
        this.noMembre = noMembre;
        this.nom = nom;
        this.prenom = prenom;
        this.lien_parente = lien_parente;
        this.MembreON=membon;
        
    }

    public String getNoMembre() {
        return noMembre;
    }

    public void setNoMembre(String noMembre) {
        this.noMembre = noMembre;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getLien_parente() {
        return lien_parente;
    }

    public void setLien_parente(String lien_parente) {
        this.lien_parente = lien_parente;
    }
    public String getMembreON() {
        return MembreON;
    }

    public void setMembreON(String membon) {
        this.MembreON = membon;
    }
 

    
  
}
