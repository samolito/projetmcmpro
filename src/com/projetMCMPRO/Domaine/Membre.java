/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projetMCMPRO.Domaine;

public class Membre {
   private String CoMembre;
   private String nomMembre;
   private String prenomMembre;
   private String titreMembre;
   //private String sexeMembre;
   private String datnaisMembre;
   private String statmatMembre;
   private String CINIF;
   private String commune;
   private String adresse;
   private String telephone;
   private String email;
   private String profession1;
   private String profession2;
   private String reference;
   private String datbap;
   private String fonction1;
   private String fonction2;
   private String relatio;
   //private String codem;
   
   public Membre()
   {
   }
   public Membre(String code)
   {
     this.CoMembre=code;  
   }
   public Membre(String cod, String nom,String pren,String titre,String datnaiss,String statmat,String cinif,String commu,String adres,String tel,String mail,String profes1,String profes2,String refere,String datbapte,String fonctio1,String fonctio2,String rela)
   {
        this.CoMembre=cod;
        this.nomMembre=nom;
        this.prenomMembre=pren;
        this.titreMembre=titre;
        //this.sexeMembre=sex;
        this.datnaisMembre=datnaiss;
        this.statmatMembre=statmat;
        this.CINIF=cinif;
        this.commune=commu;
        this.adresse=adres;
        //
        this.telephone=tel;
        this.email=mail;
        this.profession1=profes1;
        this.profession2=profes2;
        this.reference=refere;
        //
        this.datbap=datbapte;
        this.fonction1=fonctio1;
        this.fonction2=fonctio2;
        this.relatio=rela;
        
        //this.datdeces=datdec;     
   } 
   
    public String getCodeMembre() {
        return CoMembre;
    }
    public void setCodeMembre(String codMembre) {
        this.CoMembre = codMembre;
    } 

    public String getNomMembre() {
        return nomMembre;
    }
    public void setNomMembre(String nomMembre) {
        this.nomMembre = nomMembre;
    }

    public String getPrenomMembre() {
        return prenomMembre;
    }
    public void setPrenomMembre(String prenomMembre) {
        this.prenomMembre = prenomMembre;
    }

    public String getTitreMembre() {
        return titreMembre;
    }
    public void setTitreMembre(String titreMembre) {
        this.titreMembre = titreMembre;
    }
    //
    public String getDateNaissMembre() {
        return datnaisMembre;
    }
    public void setDatNaissMembre(String datnaisMembre) {
        this.datnaisMembre = datnaisMembre;
    }
    
    public String getStatMatMembre() {
        return statmatMembre;
    }
    public void setStatMatMembre(String statmatMembre) {
        this.statmatMembre = statmatMembre;
    }
    
    public String getCINIF() {
        return CINIF;
    }
    public void setCINIF(String CINIF) {
        this.CINIF = CINIF;
    }
    public String getCommune() {
        return commune;
    }
    public void setCommune(String com) {
        this.commune = com;
    }
    
    public String getAdresse() {
        return adresse;
    }
    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }
//
    public String getTelephone() {
        return telephone;
    }
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }
    
    public String getMail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    
     
    public String getProfession1() {
        return profession1;
    }
    public void setProfession1(String profession1) {
        this.profession1 = profession1;
    }
    
    public String getProfession2() {
        return profession2;
    }
    public void setProfession2(String profession2) {
        this.profession2 = profession2;
    }
   
     public String getReference() {
        return reference;
    }
    public void setReference(String reference) {
        this.reference = reference;
    }
    //
    public String getDateBapteme() {
        return datbap;
    }
    public void setDateBapteme(String datbap) {
        this.datbap = datbap;
    }
     
    public String getFonction1() {
        return fonction1;
    }
    public void setFonction1(String fonction1) {
        this.fonction1 = fonction1;
    }
    
    public String getFonction2() {
        return fonction2;
    }
    public void setFonction2(String fonction2) {
        this.fonction2 = fonction2;
    }
    
     public String getRelation() {
        return relatio;
    }
    public void setRelation(String rel) {
        this.relatio = rel;
    }
    
}
      
       
       
     