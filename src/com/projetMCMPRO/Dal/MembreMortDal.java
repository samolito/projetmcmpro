/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projetMCMPRO.Dal;
import com.projetMCMPRO.Controleur.ControleurAction;
import com.projetMCMPRO.Domaine.MembreMort;
//import static com.jardine.projetMCMPro.Vue.FenNouveauMembre.chainConnection;
//import static com.jardine.projetMCMPro.Vue.FenNouveauMembre.con;
//import static com.jardine.projetMCMPro.Vue.FenNouveauMembre.password;
//import static com.jardine.projetMCMPro.Vue.FenNouveauMembre.st;
//import static com.jardine.projetMCMPro.Vue.FenNouveauMembre.user;
import projetmcmpro.FenConnecter;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JOptionPane;



public class MembreMortDal {
   public static Connection conn=mysqlconnect.ConnectDB();
    public static Statement st,st1;
    public static  ResultSet rs1,rs;
    
     public static String code;
     public static String datefun;
     public static String datdeces;
     public static String cause;
    public static ControleurAction ctrla = new ControleurAction();
    
    public static String EnregistrerMembreMort(MembreMort membr)
   {
        
     String req = "insert into membremort values('"+membr.getIdMembre()+"','"+membr.getDateDeces()+"','"+membr.getCause()+"','"+membr.getDateFune()+"')";  
   
   int verifieE = 0;
   String rep="";
       try {
           st=conn.createStatement();
           verifieE=st.executeUpdate(req);
           if(verifieE!=0)
           {
                //*********Commencement ACTION***********      
        SimpleDateFormat d = new SimpleDateFormat ("yyyy/MM/dd" );
        SimpleDateFormat h = new SimpleDateFormat ("hh:mm:ss");
        Date currentTime_1 = new Date();
        String date_action = d.format(currentTime_1);
        String heure_action = h.format(currentTime_1);     
         String requeteE1="Select* from compte_utilisateur";  
        int verifieE1=0;
         try{
       
        Statement stateE1=conn.createStatement();
        ResultSet rssE1=stateE1.executeQuery(requeteE1);
       String codee="";
       String action="Enregistrer Deces-"+membr.getIdMembre();
        if(rssE1.next())
        {
       codee=FenConnecter.CodeUtilisateur;
       String fonction = FenConnecter.Fonction;
       verifieE1++;
       String requeteE2="Insert into Action values('"+codee+"','"+fonction+"','"+action+"','"+date_action+"','"+heure_action+"')";
        int verifiE2=0;
         try
        {
        Statement stateE2=conn.createStatement();
        verifiE2=stateE2.executeUpdate(requeteE2);
        }        
        catch(SQLException cn1)
        {
        System.out.println("Probleme Connexion, pou kisa A:"+cn1.getMessage());
        }         
        } 
    }
    catch(SQLException e1)
    {    
    }        
//***********FINISH *****************************      
               
               
           rep= "Membre modifié avec succes";
           }
       } 
       catch (SQLException e1) {
           rep=e1.getMessage();
       }
       return rep;
   } 
    
    
    // Methode modifier membre mort comme enregistrement
     static int verifieM;
     public static String ModifierMembreMort(MembreMort membr) 
    {    
        //codeMembre='"+membr.getIdMembre()+"', 
        String reket="Update membremort set date_deces='"+membr.getDateDeces()+"',cause='"+membr.getCause()+"', date_funeraille='"+membr.getDateFune()+"' where codeMembre='"+membr.getIdMembre()+"'";
        verifieM =0;
        String  repons="";
        try
        {  
        st=conn.createStatement();
        verifieM = st.executeUpdate(reket);
       if (verifieM!=0)
        {          //*********Commencement ACTION***********      
        SimpleDateFormat d = new SimpleDateFormat ("yyyy/MM/dd" );
        SimpleDateFormat h = new SimpleDateFormat ("hh:mm:ss");
        Date currentTime_1 = new Date();
        String date_action = d.format(currentTime_1);
        String heure_action = h.format(currentTime_1);
        String codee=FenConnecter.CodeUtilisateur;
        String fonction = FenConnecter.Fonction;
        String action="Enregistrement Membre-"+membr.getIdMembre();
        String reponse=ctrla.ValiderAction(codee, fonction,action, date_action, heure_action);        
        repons="Enregistrement Reussi avec succès";           
        }
        }  
        catch(SQLException cn2)
        {
        repons="Probleme Connexion, la raison:"+cn2.getMessage(); 
        }
        return repons;
   }

   
     
     //-Methode rechercher membre mort-----------------------------
     public static  boolean trouve = false;
     public static String etat="Mort";
     public static boolean RechercherMembreMort(MembreMort membr) 
    {     
      String req="Select* from membremort WHERE codeMembre='"+membr.getIdMembre()+"'"; 
      try   
      {
       st=conn.createStatement();
       rs=st.executeQuery(req);
       if(rs.next())
       {
          code=rs.getString("codeMembre");
          datefun=rs.getString("date_funeraille");
          datdeces=rs.getString("date_deces");
          cause=rs.getString("cause");
          trouve = true;
          
          }  
           //rs.close();
           //conn.close();
          return trouve;
              
          } 
            catch(SQLException se)
            {
             JOptionPane.showMessageDialog(null,"Erreur Requette"+ se.getMessage());
             }      
        return trouve;
       }
    
 //Les propriites
     
    public String getIdMembre() {
        return code;
    }
   
    public String getDateDeces() {
       return datdeces;
    }       
     
    public String getCause() {
       return cause;
    }  
     public String getDatefune() {
       return datefun;
    }
}

