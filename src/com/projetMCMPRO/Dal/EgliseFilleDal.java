/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projetMCMPRO.Dal;

import com.projetMCMPRO.Controleur.ControleurAction;
import com.projetMCMPRO.Domaine.Eglisefille;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import projetmcmpro.FenConnecter;

/**
 *
 * @author INGSAMUEL
 */
public class EgliseFilleDal {
   public static Connection conn= mysqlconnect.ConnectDB();
    public static ControleurAction ctrla=new ControleurAction();
    public static Statement st;

    
    public static String EnregistrerEglisefille(Eglisefille egl)
   {
     String req = "insert into eglisefille(nomeglisef,nompasteurf,nomdiacre,departement,commune,adressef,etat) values('"+egl.getNomEglise()+"','"+egl.getNomPasteur()+"','"+egl.getNomDiacre()+"','"+egl.getDepartement()+"','"+egl.getCommune()+"','"+egl.getAdresse()+"','"+egl.getEtat()+"')";  
   
   int verif = 0;
   String rep="";
       try {
           //con=DriverManager.getConnection(chainConnection, user, password);
           st=conn.createStatement();
           verif=st.executeUpdate(req);
          // st.close(); con.close();
           if(verif!=0)
           {
               
          //*********Commencement ACTION***********      
        SimpleDateFormat d = new SimpleDateFormat ("yyyy/MM/dd" );
        SimpleDateFormat h = new SimpleDateFormat ("hh:mm:ss");
        Date currentTime_1 = new Date();
        String date_action = d.format(currentTime_1);
        String heure_action = h.format(currentTime_1);  
        String codee=FenConnecter.CodeUtilisateur;
        String fonction = FenConnecter.Fonction;
        String action="Nouvelle Eglise fille - "+egl.getCodEglise();
        String repons=ctrla.ValiderAction(codee, fonction,action, date_action, heure_action);
        
//***********FINISH *****************************        
           rep= "Eglise Fille Enregistré";
           }
       } catch (SQLException e) {
           rep=e.getMessage();
       }
       return rep;
   }
    
     public static String ModifierEglisefille(Eglisefille egl) 
{    
         
        String reket="Update eglisefille set nomEglisef='"+egl.getNomEglise()+"',nomPasteurf='"+egl.getNomPasteur()+"',NomDiacre='"+egl.getNomDiacre()+"',Departement='"+egl.getDepartement()+"',commune='"+egl.getCommune()+"',adressef='"+egl.getAdresse()+"',etat='"+egl.getEtat()+"' where codeEglisef='"+egl.getCodEglise()+"'";
        int verif = 0;
        String  repons="";
        try
        {
        //con=DriverManager.getConnection(chainConnection, user, password);    
        st=conn.createStatement();
        verif = st.executeUpdate(reket);
       // st.close();conn.close();
        if (verif!=0)
        {
           //*********Commencement ACTION***********      
        SimpleDateFormat d = new SimpleDateFormat ("yyyy/MM/dd" );
        SimpleDateFormat h = new SimpleDateFormat ("hh:mm:ss");
        Date currentTime_1 = new Date();
        String date_action = d.format(currentTime_1);
        String heure_action = h.format(currentTime_1);  
        String codee=FenConnecter.CodeUtilisateur;
        String fonction = FenConnecter.Fonction;
        String action="Modifier Eglise fille - "+egl.getCodEglise();
       String rep=ctrla.ValiderAction(codee, fonction,action, date_action, heure_action);
        
//***********FINISH *****************************          
            
             repons="Modification Reussie";   
        }
        }  
        catch(SQLException cn2)
        {
        repons="Probleme Connexion, pou kisa:"+cn2.getMessage(); 
        }
        return repons;
        }   
     public static String SupprimerMembre(Eglisefille egl) 
    { 
       String reket="Update eglisefille set nomEglisef='"+egl.getNomEglise()+"',nomPasteurf='"+egl.getNomPasteur()+"',NomDiacre='"+egl.getNomDiacre()+"',Departement='"+egl.getDepartement()+"',commune='"+egl.getCommune()+"',adressef='"+egl.getAdresse()+"',etat='"+egl.getEtat()+"' where codeEglisef='"+egl.getCodEglise()+"'";
        int verif = 0;
        String  repons="";
        try
        {
        //con=DriverManager.getConnection(chainConnection, user, password);    
        st=conn.createStatement();
        verif = st.executeUpdate(reket);
       // st.close();conn.close();
        if (verif!=0)
        {
           //*********Commencement ACTION***********      
        SimpleDateFormat d = new SimpleDateFormat ("yyyy/MM/dd" );
        SimpleDateFormat h = new SimpleDateFormat ("hh:mm:ss");
        Date currentTime_1 = new Date();
        String date_action = d.format(currentTime_1);
        String heure_action = h.format(currentTime_1);  
        String codee=FenConnecter.CodeUtilisateur;
        String fonction = FenConnecter.Fonction;
        String action="Supprimer Eglise fille - "+egl.getCodEglise();
       String rep=ctrla.ValiderAction(codee, fonction,action, date_action, heure_action);
        
//***********FINISH *****************************          
            
             repons="Suppression Reussie";   
        }
        }  
        catch(SQLException cn2)
        {
        repons="Probleme Connexion, pou kisa:"+cn2.getMessage();  
}
  return repons;
}
     
     
}