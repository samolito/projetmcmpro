/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projetMCMPRO.Dal;

import com.projetMCMPRO.Controleur.ControleurAction;
import com.projetMCMPRO.Domaine.Offrande;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import projetmcmpro.FenConnecter;

/**
 *
 * @author INGSAMUEL
 */
public class OffrandeDal {
  public static Connection conn=mysqlconnect.ConnectDB();
    public static Statement st,st1;
    public static  ResultSet rs1,rs;
     
    public static ControleurAction ctrla = new ControleurAction();
    
    public static String EnregistrerOffrande(Offrande off)
   {
     
     String req = "insert into offrande (Nom_service,monnaie,montant,date_offrande) values('"+off.getNom_service()+"','"+off.getMonnaie()+"','"+off.getMontant()+"','"+off.getDate_off()+"')";  
   
   int verifieE = 0;
   String rep="";
       try {
           
           st=conn.createStatement();
           verifieE=st.executeUpdate(req);
           if(verifieE!=0)
           {
                //*********Commencement ACTION***********      
        SimpleDateFormat d = new SimpleDateFormat ("yyyy/MM/dd" );
        SimpleDateFormat h = new SimpleDateFormat ("hh:mm:ss");
        Date currentTime_1 = new Date();
        String date_action = d.format(currentTime_1);
        String heure_action = h.format(currentTime_1);     
         String requeteE1="Select* from compte_utilisateur";  
        int verifieE1=0;
         try{
       
        Statement stateE1=conn.createStatement();
        ResultSet rssE1=stateE1.executeQuery(requeteE1);
       String codee="";
       String action="Enregistrer Offrande:"+off.getNumero();
        if(rssE1.next())
        {
       codee=FenConnecter.CodeUtilisateur;
       String fonction = FenConnecter.Fonction;
       verifieE1++;
       String requeteE2="Insert into Action values('"+codee+"','"+fonction+"','"+action+"','"+date_action+"','"+heure_action+"')";
        int verifiE2=0;
         try
        {
        
        Statement stateE2=conn.createStatement();
        verifiE2=stateE2.executeUpdate(requeteE2);
        }        
        catch(SQLException cn1)
        {
        System.out.println("Probleme Connexion, pou kisa A:"+cn1.getMessage());
        }         
        } 
    }
    catch(SQLException e1)
    {    
    }        
//***********FINISH *****************************      
               
               
           rep= "Offrande Enregistré avec succes";
           }
       } 
       catch (SQLException e1) {
           rep=e1.getMessage();
       }
       return rep;
   } 
    public static String ModifierOffrandeDAL(Offrande off) 
    {   
        int verifieM=0;
        String reket="Update offrande set Nom_service='"+off.getNom_service()+"', monnaie='"+off.getMonnaie()+"',montant='"+off.getMontant()+"',date_offrande='"+off.getDate_off()+"' where numero='"+off.getNumero()+"'";
        //verifieM =0;
        String  repons="";
        try
        {  
        st=conn.createStatement();
        verifieM = st.executeUpdate(reket);
       if (verifieM!=0)
        {          //*********Commencement ACTION***********      
        SimpleDateFormat d = new SimpleDateFormat ("yyyy/MM/dd" );
        SimpleDateFormat h = new SimpleDateFormat ("hh:mm:ss");
        Date currentTime_1 = new Date();
        String date_action = d.format(currentTime_1);
        String heure_action = h.format(currentTime_1);
        String codee=FenConnecter.CodeUtilisateur;
        String fonction = FenConnecter.Fonction;
        String action="Modifier Offrande:"+off.getNumero();
        String reponse=ctrla.ValiderAction(codee, fonction,action, date_action, heure_action);        
        repons="Modification Reussie avec succès";           
        }
        }  
        catch(SQLException cn2)
        {
        repons="Probleme Connexion, la raison:"+cn2.getMessage(); 
        }
        return repons;
   }  
}
