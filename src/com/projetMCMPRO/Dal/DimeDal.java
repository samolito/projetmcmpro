
package com.projetMCMPRO.Dal;
import com.projetMCMPRO.Controleur.ControleurAction;
import com.projetMCMPRO.Domaine.Dime;

import projetmcmpro.FenConnecter;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;



public class DimeDal {
   public static Connection conn=mysqlconnect.ConnectDB();
    public static Statement st,st1;
    public static  ResultSet rs1,rs;
    
     public static String codemembre;
     public static String monnaie;
  
     public static String montant;
     public static String date;;
     
    public static ControleurAction ctrla = new ControleurAction();
    
    public static String EnregistrerDime(Dime dim)
   {
     
     String req = "insert into dime values('"+dim.getCodeDime()+"','"+dim.getCodeMembre()+"','"+dim.getMonnaie()+"','"+dim.getMontant()+"','"+dim.getDateDime()+"')";  
   
   int verifieE = 0;
   String rep="";
       try {
           
           st=conn.createStatement();
           verifieE=st.executeUpdate(req);
           if(verifieE!=0)
           {
                //*********Commencement ACTION***********      
        SimpleDateFormat d = new SimpleDateFormat ("yyyy/MM/dd" );
        SimpleDateFormat h = new SimpleDateFormat ("hh:mm:ss");
        Date currentTime_1 = new Date();
        String date_action = d.format(currentTime_1);
        String heure_action = h.format(currentTime_1);     
         String requeteE1="Select* from compte_utilisateur";  
        int verifieE1=0;
         try{
       
        Statement stateE1=conn.createStatement();
        ResultSet rssE1=stateE1.executeQuery(requeteE1);
       String codee="";
       String action="Enregistrer Dime:"+dim.getCodeDime();
        if(rssE1.next())
        {
       codee=FenConnecter.CodeUtilisateur;
       String fonction = FenConnecter.Fonction;
       verifieE1++;
       String requeteE2="Insert into Action values('"+codee+"','"+fonction+"','"+action+"','"+date_action+"','"+heure_action+"')";
        int verifiE2=0;
         try
        {
        
        Statement stateE2=conn.createStatement();
        verifiE2=stateE2.executeUpdate(requeteE2);
        }        
        catch(SQLException cn1)
        {
        System.out.println("Probleme Connexion, pou kisa A:"+cn1.getMessage());
        }         
        } 
    }
    catch(SQLException e1)
    {    
    }        
//***********FINISH *****************************      
               
               
           rep= "Dime Enregistré avec succes";
           }
       } 
       catch (SQLException e1) {
           rep=e1.getMessage();
       }
       return rep;
   } 
    
    
   
    //Methode modifier membre 
     static int verifieM;
     public static String ModifierDime(Dime dim) 
    {    
        String reket="Update dime set codeDime='"+dim.getCodeDime()+"',noMembre='"+dim.getCodeMembre()+"', type_monnaie='"+dim.getMonnaie()+"',montant='"+dim.getMontant()+"',date_paiement='"+dim.getDateDime()+"' where codeDime='"+dim.getCodeDime()+"'";
        verifieM =0;
        String  repons="";
        try
        {
            
        st=conn.createStatement();
        verifieM = st.executeUpdate(reket);
       if (verifieM!=0)
        {          //*********Commencement ACTION***********      
        SimpleDateFormat d = new SimpleDateFormat ("yyyy/MM/dd" );
        SimpleDateFormat h = new SimpleDateFormat ("hh:mm:ss");
        Date currentTime_1 = new Date();
        String date_action = d.format(currentTime_1);
        String heure_action = h.format(currentTime_1);
        String codee=FenConnecter.CodeUtilisateur;
        String fonction = FenConnecter.Fonction;
        String action="Modifier Dime:"+dim.getCodeDime();
        String reponse=ctrla.ValiderAction(codee, fonction,action, date_action, heure_action);        
        repons="Modification Reussie avec succès";           
        }
        }  
        catch(SQLException cn2)
        {
        repons="Probleme Connexion, la raison:"+cn2.getMessage(); 
        }
        return repons;
   }
  /*
     //-Methode rechercher membre-----------------------------
     public static  boolean trouve = false;
     public static String etat="Actif";
     public static boolean RechercherMembre(Membre membr) 
    {     
      String req="Select* from membre WHERE codeMembre='"+membr.getIdMembre()+"'"; 
      try   
      {
       con=DriverManager.getConnection(chainConnection,user,password);
       st=con.createStatement();
       rs=st.executeQuery(req);
       if(rs.next())
       {
          code=rs.getString("codeMembre");
          nom=rs.getString("nomMemb");
          prenom=rs.getString("prenomMemb");
          titre=rs.getString("titre");
          datenaiss=rs.getString("date_naissance");
          statut=rs.getString("statut_mat");
          cinif=rs.getString("cin_nif");
          commune=rs.getString("commune");
          adres=rs.getString("adresse");
          tel=rs.getString("telephone");
          imel=rs.getString("mel");
          profes1=rs.getString("profession1");
          profes2=rs.getString("profession2");
          reference=rs.getString("reference");
          dtbap=rs.getString("date_bapteme");
          fonction1=rs.getString("fonction1");
          fonction2=rs.getString("fonction2");
          relation=rs.getString("relation");
          
          trouve = true;
          
          }  
           rs.close();
           con.close();
          return trouve;
              
          } 
            catch(SQLException se)
            {
             JOptionPane.showMessageDialog(null,"Erreur Requette"+ se.getMessage());
             }      
        return trouve;
       }
    
     //-Methode supprimer membre-----------------------------
     public static  boolean trouve1 = false;
     public static String rep;
     public static String SupprimerMembre(Membre membr) 
    {     
     String Requete="DELETE from membre WHERE codeMembre='"+membr.getIdMembre()+"'";
       try{
            
            con=DriverManager.getConnection(chainConnection, user, password);    
            st=con.createStatement();
            st.executeUpdate(Requete);
            trouve1=true; 
            rep="Ce membre est mort et ne fait plus parti de la liste des membres actifs";
          }
            catch(SQLException ex){
           System.err.println(ex.getMessage());
           JOptionPane.showMessageDialog(null,"Erreur Requette"+ ex.getMessage());
        }
       return rep; 
} 
   

 //Les propriites
     
    public String getIdMembre() {
        return code;
    }
    public String getNomMembre() {
        return nom; 
    }
    public String getPrenomMembre() {
        return prenom;
    }
    public String getTitreMembre() {
        return titre;
    }
    public String getDateNaissMembre() {
        return datenaiss;
    }
    public String getStatMatMembre() {
        return statut;
    }
    public String getCINIF() {
        return cinif;
    }
    public String getCommune() {
        return commune;
    }
    public String getAdresse() {
        return adres;
    }
    public String getTelephone() {
        return tel;
    }    
    public String getMail() {
        return imel;
    }
     
    public String getProfession1() {
        return profes1;
    } 
    public String getProfession2() {
        return profes2;
    }
    public String getReference() {
        return reference;
    }
    public String getDateBapteme() {
        return dtbap;
    }
    public String getFonction1() {
        return fonction1;
    }
    public String getFonction2() {
        return fonction2;
    }
     public String getRelation() {
        return relation;
    }
    //public String getDateDeces() {
       // return datdeces;
   //         
   */  
}

