/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projetMCMPRO.Dal;

import com.projetMCMPRO.Controleur.ControleurAction;
import com.projetMCMPRO.Domaine.Eglise;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;

import java.util.Date;
import projetmcmpro.FenConnecter;

/**
 *
 * @author INGSAMUEL
 */
public class EgliseDal {
    public static Connection conn= mysqlconnect.ConnectDB();
    public static ControleurAction ctrla=new ControleurAction();
    public static Statement st;

    
    public static String EnregistrerEglise(Eglise egl)
   {
     String req = "insert into eglise values('"+egl.getCodEglise()+"','"+egl.getNomEglise()+"','"+egl.getNomPast()+"','"+egl.getAdresse()+"','"+egl.getTelephone1()+"','"+egl.getTelephone2()+"')";  
   
   int verif = 0;
   String rep="";
       try {
           //con=DriverManager.getConnection(chainConnection, user, password);
           st=conn.createStatement();
           verif=st.executeUpdate(req);
          // st.close(); con.close();
           if(verif!=0)
           {
               
          //*********Commencement ACTION***********      
        SimpleDateFormat d = new SimpleDateFormat ("yyyy/MM/dd" );
        SimpleDateFormat h = new SimpleDateFormat ("hh:mm:ss");
        Date currentTime_1 = new Date();
        String date_action = d.format(currentTime_1);
        String heure_action = h.format(currentTime_1);  
        String codee=FenConnecter.CodeUtilisateur;
        String fonction = FenConnecter.Fonction;
        String action="Nouvelle Eglise - "+egl.getCodEglise();
        String repons=ctrla.ValiderAction(codee, fonction,action, date_action, heure_action);
        
//***********FINISH *****************************        
           rep= "Eglise Enregistré";
           }
       } catch (SQLException e) {
           rep=e.getMessage();
       }
       return rep;
   }
    
     public static String ModifierEglise(Eglise egl) 
{    
         
        String reket="Update eglise set nomEglise='"+egl.getNomEglise()+"',nomPasteur='"+egl.getNomPast()+"',adresse='"+egl.getAdresse()+"',telephone1='"+egl.getTelephone1()+"',telephone2='"+egl.getTelephone2()+"' where codeEglise='"+egl.getCodEglise()+"'";
        int verif = 0;
        String  repons="";
        try
        {
        //con=DriverManager.getConnection(chainConnection, user, password);    
        st=conn.createStatement();
        verif = st.executeUpdate(reket);
        //st.close();conn.close();
        if (verif!=0)
        {
           //*********Commencement ACTION***********      
        SimpleDateFormat d = new SimpleDateFormat ("yyyy/MM/dd" );
        SimpleDateFormat h = new SimpleDateFormat ("hh:mm:ss");
        Date currentTime_1 = new Date();
        String date_action = d.format(currentTime_1);
        String heure_action = h.format(currentTime_1);  
        String codee=FenConnecter.CodeUtilisateur;
        String fonction = FenConnecter.Fonction;
        String action="Modifier Eglise - "+egl.getCodEglise();
       String rep=ctrla.ValiderAction(codee, fonction,action, date_action, heure_action);
        
//***********FINISH *****************************          
            
             repons="Modification Reussie";   
        }
        }  
        catch(SQLException cn2)
        {
        repons="Probleme Connexion, pou kisa:"+cn2.getMessage(); 
        }
        return repons;
        } 
    
}
