/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projetMCMPRO.Dal;
import com.projetMCMPRO.Controleur.ControleurAction;
import projetmcmpro.FenConnecter;
import com.projetMCMPRO.Domaine.CompteUtil;
//import com.jardine.projetMCMPro.Vue.FenNouveauUtilisateur;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
//import projetsystra.FenConnecter;

public  class CompteUtilDal{
    public static Connection conn=mysqlconnect.ConnectDB();
    public static ControleurAction ctrlA=new ControleurAction();
    public static Statement st;
    
    public static  String EnregistrerCompteUtil(CompteUtil cmp)
   {
     String req = "insert into compte_utilisateur values('"+cmp.getCodeUtil()+"','"+cmp.getNom()+"','"+cmp.getPrenom()+"','"+cmp.getCin_Nif()+"','"+cmp.getAdresse()+"','"+cmp.getTelephone()+"','"+cmp.getFonction()+"','"+cmp.getNom_utilisateur()+"','"+cmp.getMot_de_passe()+"','"+cmp.getEtat_Compte()+"')";  
   
   int verif = 0;
   String rep="";
       try {
           
            st=conn.createStatement();
          //st=maConnection.ObtenirConnxion().createStatement();
           verif=st.executeUpdate(req);
           if(verif!=0)
           {
             //*********Commencement ACTION***********      
        SimpleDateFormat d = new SimpleDateFormat ("yyyy/MM/dd" );
        SimpleDateFormat h = new SimpleDateFormat ("hh:mm:ss");
        Date currentTime_1 = new Date();
        String date_action = d.format(currentTime_1);
        String heure_action = h.format(currentTime_1);
        String codee=FenConnecter.CodeUtilisateur;
        String fonction = FenConnecter.Fonction;
        String action="Ajouter Nouveau Utilisateur - "+cmp.getCodeUtil();
        String repons=ctrlA.ValiderAction(codee, fonction,action, date_action, heure_action);
//***********FINISH *****************************             
               
           rep= "Utilisateur Enregistré";
           }
       } 
       catch (Exception e) 
       {
           rep=e.getMessage();
       }
       return rep;
   } 
    
    public static String ModifierCompteUtil(CompteUtil cmp) 
{    
         
        String reket="Update compte_utilisateur set  nom='"+cmp.getNom()+"',prenom='"+cmp.getPrenom()+"',cin_nif='"+cmp.getCin_Nif()+"',adresse='"+cmp.getAdresse()+"',telephone='"+cmp.getTelephone()+"', fonction='"+cmp.getFonction()+"',userName='"+cmp.getNom_utilisateur()+"',password='"+cmp.getMot_de_passe()+"',etat_compte='"+cmp.getEtat_Compte()+"' where codeUtilisateur='"+cmp.getCodeUtil()+"'";
        int verif = 0;
        String  repons="";
        try
        {    
        st=conn.createStatement();
        //st=maConnection.ObtenirConnxion().createStatement();
        verif = st.executeUpdate(reket);  
        //st.close();con.close();
         if (verif!=0)
        {
            //*********Commencement ACTION***********      
        SimpleDateFormat d = new SimpleDateFormat ("yyyy/MM/dd" );
        SimpleDateFormat h = new SimpleDateFormat ("hh:mm:ss");
        Date currentTime_1 = new Date();
        String date_action = d.format(currentTime_1);
        String heure_action = h.format(currentTime_1);
        String codee=FenConnecter.CodeUtilisateur;
        String fonction = FenConnecter.Fonction;
        String action="Modifier Utilisateur - "+cmp.getCodeUtil();
        String rep=ctrlA.ValiderAction(codee, fonction,action, date_action, heure_action);
//***********FINISH *****************************             
             repons="Modification Reussie";   
        }
        }  
        catch(SQLException cn2)
        {
        repons="Probleme Connexion, pou kisa:"+cn2.getMessage(); 
        }
        return repons;
   } 
     
     
 public static String SupprimerCompteUtil(CompteUtil cmp) 
{    
         
        String reket="Update compte_utilisateur set  nom='"+cmp.getNom()+"',prenom='"+cmp.getPrenom()+"',cin_nif='"+cmp.getCin_Nif()+"',adresse='"+cmp.getAdresse()+"',telephone='"+cmp.getTelephone()+"', fonction='"+cmp.getFonction()+"',userName='"+cmp.getNom_utilisateur()+"',password='"+cmp.getMot_de_passe()+"',etat_compte='"+cmp.getEtat_Compte()+"' where codeUtilisateur='"+cmp.getCodeUtil()+"'";
        int verif = 0;
        String  repons="";
        try
        {   
        st=conn.createStatement();
        verif = st.executeUpdate(reket);
         
        //Statement stt;
        //st=maConnection.ObtenirConnxion().createStatement();
       if (verif!=0)
        {
            //*********Commencement ACTION***********      
        SimpleDateFormat d = new SimpleDateFormat ("yyyy/MM/dd" );
        SimpleDateFormat h = new SimpleDateFormat ("hh:mm:ss");
        Date currentTime_1 = new Date();
        String date_action = d.format(currentTime_1);
        String heure_action = h.format(currentTime_1);     
         String requet="Select* from compte_utilisateur";  
        int ver=0;
         try{
       
        Statement sta=conn.createStatement();
        ResultSet rs=sta.executeQuery(reket);
        
     
        //=con.createStatement();
        //Statement sta=maConnection.ObtenirConnxion().createStatement();
        //ResultSet rs=sta.executeQuery(reket);
       // System.out.println("Code\t\t \tNom\t\t\tPrenom\t\t\tAdresse \t\t\t Telephone\t\t\t Nom_Utilisateur\t\t\t Mot_de_passe\t\t\tFonction\t\t\tEtat_de_compte\t\t\tDate_de_connexion");
       String codee="";
       String action="Supprimer Utilisateur"+cmp.getCodeUtil();
        if(rs.next())
        {
       codee=FenConnecter.CodeUtilisateur;
       String fonction = FenConnecter.Fonction;
       ver++;
       String requ="Insert into Action values('"+codee+"','"+fonction+"','"+action+"','"+date_action+"','"+heure_action+"')";
        int veri=0;
         try
        {
        Statement stt=conn.createStatement();
        
        //Statement stt;
        //stt=maConnection.ObtenirConnxion().createStatement();
        veri=stt.executeUpdate(requ);
        }        
        catch(SQLException cn1)
        {
        System.out.println("Probleme Connexion, pou kisa A:"+cn1.getMessage());
        }         
        } 
    }catch(SQLException e)
    {    
    }        
//***********FINISH ***************************** 
            
             repons="Suppression Reussie";   
        }
        }  
        catch(SQLException cn2)
        {
        repons="Probleme Connexion, pou kisa:"+cn2.getMessage(); 
        }
        return repons;
        }
}

