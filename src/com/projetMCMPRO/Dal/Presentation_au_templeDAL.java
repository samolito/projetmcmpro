/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projetMCMPRO.Dal;

import com.projetMCMPRO.Controleur.ControleurAction;
import com.projetMCMPRO.Domaine.Presentation_au_temple;
import java.sql.Connection;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import projetmcmpro.FenConnecter;

/**
 *
 * @author INGSAMUEL
 */
public class Presentation_au_templeDAL {
   public static Connection conn=mysqlconnect.ConnectDB();
    public static ControleurAction ctrlA=new ControleurAction();
    public static Statement st;
    
    public static  String EnregistrerPresent_au_temple(Presentation_au_temple pat)
   {
     String req = "insert into presentation_au_temple values('"+pat.getMembre()+"','"+pat.getNoMembre()+"','"+pat.getNomEnfant()+"','"+pat.getPrenomEnfant()+"','"+pat.getDate_naissance()+"','"+pat.getlieu_naissance()+"','"+pat.getNompere()+"','"+pat.getNommere()+"','"+pat.getDate_presentation()+"')";  
   
   int verif = 0;
   String rep="";
       try {
           
            st=conn.createStatement();
          //st=maConnection.ObtenirConnxion().createStatement();
           verif=st.executeUpdate(req);
           if(verif!=0)
           {
             //*********Commencement ACTION***********      
        SimpleDateFormat d = new SimpleDateFormat ("yyyy/MM/dd" );
        SimpleDateFormat h = new SimpleDateFormat ("hh:mm:ss");
        Date currentTime_1 = new Date();
        String date_action = d.format(currentTime_1);
        String heure_action = h.format(currentTime_1);
        String codee=FenConnecter.CodeUtilisateur;
        String fonction = FenConnecter.Fonction;
        String action="Ajouter Nouvean Presentation - "+pat.getNoMembre();
        String repons=ctrlA.ValiderAction(codee, fonction,action, date_action, heure_action);
//***********FINISH *****************************             
               
           rep= "Presentation Enregistré";
           }
       } 
       catch (Exception e) 
       {
           rep=e.getMessage();
       }
       return rep;
   }   
    public static  String ModifierPresentation_au_temple(Presentation_au_temple pat)
   {
     String reket="Update presentation_au_temple set Membre='"+pat.getMembre()+"',nomenfant='"+pat.getNomEnfant()+"',prenomenfant='"+pat.getPrenomEnfant()+"',date_naissance='"+pat.getDate_naissance()+"',lieunaissance='"+pat.getlieu_naissance()+"',nompere='"+pat.getNompere()+"',nommere='"+pat.getNommere()+"',date_present='"+pat.getDate_presentation()+"' where noMembre='"+pat.getNoMembre()+"'";
   
   int verif = 0;
   String rep="";
       try {
           
            st=conn.createStatement();
          //st=maConnection.ObtenirConnxion().createStatement();
           verif=st.executeUpdate(reket);
           if(verif!=0)
           {
             //*********Commencement ACTION***********      
        SimpleDateFormat d = new SimpleDateFormat ("yyyy/MM/dd" );
        SimpleDateFormat h = new SimpleDateFormat ("hh:mm:ss");
        Date currentTime_1 = new Date();
        String date_action = d.format(currentTime_1);
        String heure_action = h.format(currentTime_1);
        String codee=FenConnecter.CodeUtilisateur;
        String fonction = FenConnecter.Fonction;
        String action="Modifier Presentation- "+pat.getNoMembre();
        String repons=ctrlA.ValiderAction(codee, fonction,action, date_action, heure_action);
//***********FINISH *****************************             
               
           rep= "Presentation Enregistré";
           }
       } 
       catch (Exception e) 
       {
           rep=e.getMessage();
       }
       return rep;
   }     
}
