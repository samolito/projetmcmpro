/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projetMCMPRO.Dal;
//import com.jardine.projetMCMPro.Domaine;
import java.sql.*;

/**
 *
 * @author Prisca
 */
public class  Connexion {
    
    public static String urlPilote="com.mysql.jdbc.Driver";//chemin pour charger le pilote
    public static String urlBaseDonnees="jdbc:mysql://localhost:3306/db_mcmpro";//chemin de connexion a la base de donnees.
    public static Connection con;
    
    
    public  Connexion() {
    //chargement de notre pilote
    try{
      Class.forName(urlPilote);
       System.out.println("OK chargement de pilote réussie...");
       }
    catch(ClassNotFoundException e)
    {
     System.err.print("ClassNotFoundException:");
     System.err.println(e.getMessage());
     } 
    //Connexion a la base de donnees.
     try{
      con = DriverManager.getConnection(urlBaseDonnees,"root","agritech");
      System.out.println("OK connexion a la base de donnees réussie...");
     }
    catch(SQLException ex){
    System.out.println(ex);
    }
    
   }

  public  Connection ObtenirConnxion(){
   return con;
   }      
}
