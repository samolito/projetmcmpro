-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.14 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for dbmcmpro
CREATE DATABASE IF NOT EXISTS `dbmcmpro` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `dbmcmpro`;


-- Dumping structure for table dbmcmpro.action
CREATE TABLE IF NOT EXISTS `action` (
  `codeutil` varchar(50) DEFAULT NULL,
  `fonction` varchar(50) DEFAULT NULL,
  `actions` varchar(50) DEFAULT NULL,
  `dateaction` varchar(50) DEFAULT NULL,
  `heureaction` varchar(50) DEFAULT NULL,
  KEY `FK_action_compte_utilisateur` (`codeutil`),
  CONSTRAINT `FK_action_compte_utilisateur` FOREIGN KEY (`codeutil`) REFERENCES `compte_utilisateur` (`codeUtilisateur`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table dbmcmpro.action: ~0 rows (approximately)
/*!40000 ALTER TABLE `action` DISABLE KEYS */;
INSERT INTO `action` (`codeutil`, `fonction`, `actions`, `dateaction`, `heureaction`) VALUES
	('pjs001', 'Administrateur', 'Connecter', '2019/02/02', '11:56:15'),
	('pjs001', 'Administrateur', 'Deconnecter', '2019/02/02', '11:56:49'),
	('pjs001', 'Pasteur', 'Connecter', '2019/02/02', '11:57:08'),
	('pjs001', 'Pasteur', 'Nouvelle Eglise - ASS-E0L-001', '2019/02/02', '11:58:09'),
	('pjs001', 'Pasteur', 'Ajouter Nouveau Utilisateur - JeaHE-4412', '2019/02/02', '11:59:36'),
	('pjs001', 'Pasteur', 'Enregistrer Membre-ASS-JAR-002', '2019/02/03', '12:01:53');
/*!40000 ALTER TABLE `action` ENABLE KEYS */;


-- Dumping structure for table dbmcmpro.composant_foyer
CREATE TABLE IF NOT EXISTS `composant_foyer` (
  `noMembre` varchar(50) DEFAULT NULL,
  `nom` varchar(50) DEFAULT NULL,
  `prenom` varchar(50) DEFAULT NULL,
  `lien_parente` varchar(50) DEFAULT NULL,
  `MembreOuiNon` varchar(50) DEFAULT NULL,
  KEY `FK_composant_foyer_membre` (`noMembre`),
  CONSTRAINT `FK_composant_foyer_membre` FOREIGN KEY (`noMembre`) REFERENCES `membre` (`codeMembre`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table dbmcmpro.composant_foyer: ~0 rows (approximately)
/*!40000 ALTER TABLE `composant_foyer` DISABLE KEYS */;
/*!40000 ALTER TABLE `composant_foyer` ENABLE KEYS */;


-- Dumping structure for table dbmcmpro.compte_utilisateur
CREATE TABLE IF NOT EXISTS `compte_utilisateur` (
  `codeUtilisateur` varchar(50) NOT NULL,
  `nom` varchar(50) DEFAULT NULL,
  `prenom` varchar(50) DEFAULT NULL,
  `cin_nif` varchar(50) DEFAULT NULL,
  `adresse` varchar(50) DEFAULT NULL,
  `telephone` varchar(50) DEFAULT NULL,
  `fonction` varchar(50) DEFAULT NULL,
  `userName` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `etat_compte` varchar(50) NOT NULL,
  PRIMARY KEY (`codeUtilisateur`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table dbmcmpro.compte_utilisateur: ~1 rows (approximately)
/*!40000 ALTER TABLE `compte_utilisateur` DISABLE KEYS */;
INSERT INTO `compte_utilisateur` (`codeUtilisateur`, `nom`, `prenom`, `cin_nif`, `adresse`, `telephone`, `fonction`, `userName`, `password`, `etat_compte`) VALUES
	('JeaHE-4412', 'HEROLD', 'Jean Louis', '101-928-384-8', 'Mahotiere', '(509)7738-3747', 'Pasteur', 'herold', '123456789', 'actif'),
	('pjs001', 'PIERRE', 'Jean Samuel', '091-109-209-9', 'Hanoi', '848771672901', 'Pasteur', 'samolito', '123456789', 'actif');
/*!40000 ALTER TABLE `compte_utilisateur` ENABLE KEYS */;


-- Dumping structure for table dbmcmpro.dime
CREATE TABLE IF NOT EXISTS `dime` (
  `codeDime` varchar(50) NOT NULL,
  `noMembre` varchar(50) DEFAULT NULL,
  `type_monnaie` varchar(50) DEFAULT NULL,
  `montant` double DEFAULT NULL,
  `date_paiement` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`codeDime`),
  KEY `FK_dime_membre` (`noMembre`),
  CONSTRAINT `FK_dime_membre` FOREIGN KEY (`noMembre`) REFERENCES `membre` (`codeMembre`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table dbmcmpro.dime: ~0 rows (approximately)
/*!40000 ALTER TABLE `dime` DISABLE KEYS */;
/*!40000 ALTER TABLE `dime` ENABLE KEYS */;


-- Dumping structure for table dbmcmpro.eglise
CREATE TABLE IF NOT EXISTS `eglise` (
  `codeEglise` varchar(50) NOT NULL,
  `nomEglise` varchar(50) NOT NULL,
  `nomPasteur` varchar(50) NOT NULL,
  `adresse` varchar(50) NOT NULL,
  `telephone1` varchar(50) NOT NULL,
  `telephone2` varchar(50) NOT NULL,
  PRIMARY KEY (`codeEglise`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table dbmcmpro.eglise: ~0 rows (approximately)
/*!40000 ALTER TABLE `eglise` DISABLE KEYS */;
INSERT INTO `eglise` (`codeEglise`, `nomEglise`, `nomPasteur`, `adresse`, `telephone1`, `telephone2`) VALUES
	('ASS-E0L-001', 'EGLISE DE DIEU DE LA RUE DU CENTRE', 'Pasteur Louis Reniel', 'Rue centre- Port-au-Prince', '(509)4617-1722', '(509)    -    ');
/*!40000 ALTER TABLE `eglise` ENABLE KEYS */;


-- Dumping structure for table dbmcmpro.eglisefille
CREATE TABLE IF NOT EXISTS `eglisefille` (
  `codeEglisef` varchar(50) NOT NULL,
  `nomEglisef` varchar(50) DEFAULT NULL,
  `nomPasteurf` varchar(50) DEFAULT NULL,
  `NomDiacre` varchar(50) DEFAULT NULL,
  `Departement` varchar(50) DEFAULT NULL,
  `commune` varchar(50) DEFAULT NULL,
  `adressef` varchar(50) DEFAULT NULL,
  `etat` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`codeEglisef`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table dbmcmpro.eglisefille: ~0 rows (approximately)
/*!40000 ALTER TABLE `eglisefille` DISABLE KEYS */;
/*!40000 ALTER TABLE `eglisefille` ENABLE KEYS */;


-- Dumping structure for table dbmcmpro.foyer
CREATE TABLE IF NOT EXISTS `foyer` (
  `noMembre` varchar(50) DEFAULT NULL,
  `nomMemb` varchar(50) DEFAULT NULL,
  `prenomMemb` varchar(50) DEFAULT NULL,
  `statut_mat` varchar(50) DEFAULT NULL,
  `telephone` varchar(50) DEFAULT NULL,
  `nomconjoint` varchar(50) DEFAULT NULL,
  `nbreenfant` int(11) DEFAULT NULL,
  `nb_pers_foyer` int(11) DEFAULT NULL,
  KEY `FK_foyer_membre` (`noMembre`),
  CONSTRAINT `FK_foyer_membre` FOREIGN KEY (`noMembre`) REFERENCES `membre` (`codeMembre`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table dbmcmpro.foyer: ~0 rows (approximately)
/*!40000 ALTER TABLE `foyer` DISABLE KEYS */;
/*!40000 ALTER TABLE `foyer` ENABLE KEYS */;


-- Dumping structure for table dbmcmpro.membre
CREATE TABLE IF NOT EXISTS `membre` (
  `codeMembre` varchar(50) NOT NULL,
  `nomMemb` varchar(50) DEFAULT NULL,
  `prenomMemb` varchar(50) DEFAULT NULL,
  `titre` varchar(50) DEFAULT NULL,
  `date_naissance` varchar(50) DEFAULT NULL,
  `statut_mat` varchar(50) DEFAULT NULL,
  `cin_nif` varchar(50) DEFAULT NULL,
  `commune` varchar(50) DEFAULT NULL,
  `adresse` varchar(50) DEFAULT NULL,
  `telephone` varchar(50) DEFAULT NULL,
  `mail` varchar(50) DEFAULT NULL,
  `profession1` varchar(50) DEFAULT NULL,
  `profession2` varchar(50) DEFAULT NULL,
  `reference` varchar(50) DEFAULT NULL,
  `date_bapteme` varchar(50) DEFAULT NULL,
  `fonction1` varchar(50) DEFAULT NULL,
  `fonction2` varchar(50) DEFAULT NULL,
  `relation` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`codeMembre`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table dbmcmpro.membre: ~0 rows (approximately)
/*!40000 ALTER TABLE `membre` DISABLE KEYS */;
INSERT INTO `membre` (`codeMembre`, `nomMemb`, `prenomMemb`, `titre`, `date_naissance`, `statut_mat`, `cin_nif`, `commune`, `adresse`, `telephone`, `mail`, `profession1`, `profession2`, `reference`, `date_bapteme`, `fonction1`, `fonction2`, `relation`) VALUES
	('ASS-JAR-002', 'JEROME', 'Matilde', 'Soeur', '1989-02-10', 'Marie(e)', '123-456-789-0', 'Croix-des-Bouquet', 'La plaine', '(509)4718-2773', 'matilde@gmail.com', 'Infirmiere', 'Mere de famille', 'Frere Samuel', '2010-02-11', 'Consiège', 'Dame', 'Actif'),
	('memb-001', 'PIERRE', 'Jean Samuel', 'pasteur', '1992-11-25', 'celibataire', '001-0012-102-9', 'Hanoi', 'My dinh', '8487717238', 'samolito92@gmail.com', 'Informaticien', 'Footballeur', 'Frisnel', '2001-10-19', 'Pasteur', 'President', 'Fort');
/*!40000 ALTER TABLE `membre` ENABLE KEYS */;


-- Dumping structure for table dbmcmpro.membremort
CREATE TABLE IF NOT EXISTS `membremort` (
  `codeMembre` varchar(50) DEFAULT NULL,
  `date_deces` varchar(50) DEFAULT NULL,
  `cause` varchar(50) DEFAULT NULL,
  `date_funeraille` varchar(50) DEFAULT NULL,
  KEY `FK_membremort_membre` (`codeMembre`),
  CONSTRAINT `FK_membremort_membre` FOREIGN KEY (`codeMembre`) REFERENCES `membre` (`codeMembre`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table dbmcmpro.membremort: ~0 rows (approximately)
/*!40000 ALTER TABLE `membremort` DISABLE KEYS */;
/*!40000 ALTER TABLE `membremort` ENABLE KEYS */;


-- Dumping structure for table dbmcmpro.offrande
CREATE TABLE IF NOT EXISTS `offrande` (
  `numero` int(11) NOT NULL AUTO_INCREMENT,
  `Nom_service` varchar(50) DEFAULT NULL,
  `monnaie` varchar(50) DEFAULT NULL,
  `montant` double DEFAULT NULL,
  `date_offrande` double DEFAULT NULL,
  PRIMARY KEY (`numero`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table dbmcmpro.offrande: ~0 rows (approximately)
/*!40000 ALTER TABLE `offrande` DISABLE KEYS */;
/*!40000 ALTER TABLE `offrande` ENABLE KEYS */;


-- Dumping structure for table dbmcmpro.presentation_au_temple
CREATE TABLE IF NOT EXISTS `presentation_au_temple` (
  `noMembre` varchar(50) DEFAULT NULL,
  `nomenfant` varchar(50) DEFAULT NULL,
  `prenomenfant` varchar(50) DEFAULT NULL,
  `date_naissance` varchar(50) DEFAULT NULL,
  `lieunaissance` varchar(50) DEFAULT NULL,
  `nompere` varchar(50) DEFAULT NULL,
  `nommere` varchar(50) DEFAULT NULL,
  KEY `FK_presentation_au_temple_membre` (`noMembre`),
  CONSTRAINT `FK_presentation_au_temple_membre` FOREIGN KEY (`noMembre`) REFERENCES `membre` (`codeMembre`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table dbmcmpro.presentation_au_temple: ~0 rows (approximately)
/*!40000 ALTER TABLE `presentation_au_temple` DISABLE KEYS */;
/*!40000 ALTER TABLE `presentation_au_temple` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
